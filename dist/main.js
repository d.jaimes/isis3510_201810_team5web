(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error('Cannot find module "' + req + '".');
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _componentes_home_page_home_page_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./componentes/home-page/home-page.component */ "./src/app/componentes/home-page/home-page.component.ts");
/* harmony import */ var _componentes_login_page_login_page_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./componentes/login-page/login-page.component */ "./src/app/componentes/login-page/login-page.component.ts");
/* harmony import */ var _componentes_assistance_assistance_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./componentes/assistance/assistance.component */ "./src/app/componentes/assistance/assistance.component.ts");
/* harmony import */ var _componentes_not_found_page_not_found_page_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./componentes/not-found-page/not-found-page.component */ "./src/app/componentes/not-found-page/not-found-page.component.ts");
/* harmony import */ var _componentes_privado_page_privado_page_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./componentes/privado-page/privado-page.component */ "./src/app/componentes/privado-page/privado-page.component.ts");
/* harmony import */ var _componentes_register_page_register_page_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./componentes/register-page/register-page.component */ "./src/app/componentes/register-page/register-page.component.ts");
/* harmony import */ var _componentes_estadisticas_estadisticas_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./componentes/estadisticas/estadisticas.component */ "./src/app/componentes/estadisticas/estadisticas.component.ts");
/* harmony import */ var _componentes_mapeo_mapeo_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./componentes/mapeo/mapeo.component */ "./src/app/componentes/mapeo/mapeo.component.ts");
/* harmony import */ var _componentes_supermarket_detail_supermarket_detail_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./componentes/supermarket-detail/supermarket-detail.component */ "./src/app/componentes/supermarket-detail/supermarket-detail.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};











var routes = [
    { path: '', component: _componentes_home_page_home_page_component__WEBPACK_IMPORTED_MODULE_2__["HomePageComponent"] },
    { path: 'login', component: _componentes_login_page_login_page_component__WEBPACK_IMPORTED_MODULE_3__["LoginPageComponent"] },
    { path: 'register', component: _componentes_register_page_register_page_component__WEBPACK_IMPORTED_MODULE_7__["RegisterPageComponent"] },
    { path: 'private', component: _componentes_privado_page_privado_page_component__WEBPACK_IMPORTED_MODULE_6__["PrivadoPageComponent"] },
    { path: 'assistance', component: _componentes_assistance_assistance_component__WEBPACK_IMPORTED_MODULE_4__["AssistanceComponent"] },
    { path: 'estadistica', component: _componentes_estadisticas_estadisticas_component__WEBPACK_IMPORTED_MODULE_8__["EstadisticasComponent"] },
    { path: 'mapeo', component: _componentes_mapeo_mapeo_component__WEBPACK_IMPORTED_MODULE_9__["MapeoComponent"] },
    { path: 'supermaket_detail', component: _componentes_supermarket_detail_supermarket_detail_component__WEBPACK_IMPORTED_MODULE_10__["SupermarketDetailComponent"] },
    { path: '**', component: _componentes_not_found_page_not_found_page_component__WEBPACK_IMPORTED_MODULE_5__["NotFoundPageComponent"] }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]],
            declarations: []
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "h1, h2, h3 {\n\tmargin: 0;\n\tpadding: 0;\n\ttext-transform: uppercase;\n\tfont-weight: 300;\n\tcolor: #000000;\n\t-webkit-text-decoration-color: #000000;\n\t        text-decoration-color: #000000;\n}\n\nh1 {\n    font-size: 2em;\n    text-align: center;\n}\n\nh2 {\n\n\tfont-size: 1.3em;\n}\n\n#three-column {\n\toverflow: hidden;\n\tpadding: 10px 0px 50px 0px;\n}\n\n#three-column .tbox1 {\n\tfloat: left;\n\twidth: 384px;\n\tmargin-right: 20px;\n}\n\n#three-column .tbox2 {\n\tfloat: left;\n\twidth: 384px;\n}\n\n#three-column .tbox3 {\n\tfloat: right;\n\twidth: 384px;\n}\n\n.box-style {\n\tbackground: #FFFFFF;\n\ttext-align: center;\n\tborder-radius: 15px;\n\t\n\n}\n\n.button\n{\n\tdisplay: inline-block;\n\tmargin: 1em 0em;\n\tbackground: #000;\n\tpadding: 1em 2em;\n\tcolor: #FFFFFF;\n}\n"

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-navbar>  </app-navbar>\n<router-outlet></router-outlet>\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _firestore_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./firestore.service */ "./src/app/firestore.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AppComponent = /** @class */ (function () {
    function AppComponent(fireStoreService) {
        //this.fireStoreService
        // this.getMensajes();
        this.fireStoreService = fireStoreService;
        this.title = 'app';
        this.mensajes = [];
    }
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        }),
        __metadata("design:paramtypes", [_firestore_service__WEBPACK_IMPORTED_MODULE_1__["FireStoreService"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var angular_webstorage_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! angular-webstorage-service */ "./node_modules/angular-webstorage-service/bundles/angular-webstorage-service.es5.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/index.js");
/* harmony import */ var _angular_fire__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/fire */ "./node_modules/@angular/fire/index.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _firestore_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./firestore.service */ "./src/app/firestore.service.ts");
/* harmony import */ var angularfire2_firestore__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! angularfire2/firestore */ "./node_modules/angularfire2/firestore/index.js");
/* harmony import */ var angularfire2_firestore__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(angularfire2_firestore__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var angularfire2_auth__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! angularfire2/auth */ "./node_modules/angularfire2/auth/index.js");
/* harmony import */ var angularfire2_auth__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(angularfire2_auth__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _componentes_home_page_home_page_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./componentes/home-page/home-page.component */ "./src/app/componentes/home-page/home-page.component.ts");
/* harmony import */ var _componentes_navbar_navbar_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./componentes/navbar/navbar.component */ "./src/app/componentes/navbar/navbar.component.ts");
/* harmony import */ var _componentes_register_page_register_page_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./componentes/register-page/register-page.component */ "./src/app/componentes/register-page/register-page.component.ts");
/* harmony import */ var _componentes_login_page_login_page_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./componentes/login-page/login-page.component */ "./src/app/componentes/login-page/login-page.component.ts");
/* harmony import */ var _componentes_privado_page_privado_page_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./componentes/privado-page/privado-page.component */ "./src/app/componentes/privado-page/privado-page.component.ts");
/* harmony import */ var _componentes_not_found_page_not_found_page_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./componentes/not-found-page/not-found-page.component */ "./src/app/componentes/not-found-page/not-found-page.component.ts");
/* harmony import */ var _componentes_assistance_assistance_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./componentes/assistance/assistance.component */ "./src/app/componentes/assistance/assistance.component.ts");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! .//app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _componentes_estadisticas_estadisticas_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./componentes/estadisticas/estadisticas.component */ "./src/app/componentes/estadisticas/estadisticas.component.ts");
/* harmony import */ var _componentes_mapeo_mapeo_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./componentes/mapeo/mapeo.component */ "./src/app/componentes/mapeo/mapeo.component.ts");
/* harmony import */ var _componentes_supermarket_detail_supermarket_detail_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./componentes/supermarket-detail/supermarket-detail.component */ "./src/app/componentes/supermarket-detail/supermarket-detail.component.ts");
/* harmony import */ var _servicios_auth_service__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./servicios/auth.service */ "./src/app/servicios/auth.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _componentes_usuario_usuario_component__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./componentes/usuario/usuario.component */ "./src/app/componentes/usuario/usuario.component.ts");
/* harmony import */ var angularfire2_database__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! angularfire2/database */ "./node_modules/angularfire2/database/index.js");
/* harmony import */ var angularfire2_database__WEBPACK_IMPORTED_MODULE_24___default = /*#__PURE__*/__webpack_require__.n(angularfire2_database__WEBPACK_IMPORTED_MODULE_24__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
























//import {AngularFireModule} from 'angularfire2';

var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"],
                _componentes_home_page_home_page_component__WEBPACK_IMPORTED_MODULE_10__["HomePageComponent"],
                _componentes_navbar_navbar_component__WEBPACK_IMPORTED_MODULE_11__["NavbarComponent"],
                _componentes_register_page_register_page_component__WEBPACK_IMPORTED_MODULE_12__["RegisterPageComponent"],
                _componentes_login_page_login_page_component__WEBPACK_IMPORTED_MODULE_13__["LoginPageComponent"],
                _componentes_privado_page_privado_page_component__WEBPACK_IMPORTED_MODULE_14__["PrivadoPageComponent"],
                _componentes_not_found_page_not_found_page_component__WEBPACK_IMPORTED_MODULE_15__["NotFoundPageComponent"],
                _componentes_assistance_assistance_component__WEBPACK_IMPORTED_MODULE_16__["AssistanceComponent"],
                _componentes_estadisticas_estadisticas_component__WEBPACK_IMPORTED_MODULE_18__["EstadisticasComponent"],
                _componentes_mapeo_mapeo_component__WEBPACK_IMPORTED_MODULE_19__["MapeoComponent"],
                _componentes_usuario_usuario_component__WEBPACK_IMPORTED_MODULE_23__["UsuarioComponent"],
                _componentes_supermarket_detail_supermarket_detail_component__WEBPACK_IMPORTED_MODULE_20__["SupermarketDetailComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                _angular_fire__WEBPACK_IMPORTED_MODULE_5__["AngularFireModule"].initializeApp(_environments_environment__WEBPACK_IMPORTED_MODULE_6__["environment"].firebase),
                angularfire2_firestore__WEBPACK_IMPORTED_MODULE_8__["AngularFirestoreModule"],
                angularfire2_auth__WEBPACK_IMPORTED_MODULE_9__["AngularFireAuthModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_17__["AppRoutingModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_22__["FormsModule"],
                angular_webstorage_service__WEBPACK_IMPORTED_MODULE_2__["StorageServiceModule"]
            ],
            providers: [
                _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_4__["AngularFirestore"],
                _firestore_service__WEBPACK_IMPORTED_MODULE_7__["FireStoreService"],
                _servicios_auth_service__WEBPACK_IMPORTED_MODULE_21__["AuthService"],
                angularfire2_database__WEBPACK_IMPORTED_MODULE_24__["AngularFireDatabase"]
            ],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/componentes/assistance/assistance.component.css":
/*!*****************************************************************!*\
  !*** ./src/app/componentes/assistance/assistance.component.css ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".assistance-container {\n  margin-top: 20px;\n}\n\nh1, h2, h3 {\n\tmargin: 0;\n\tpadding: 0;\n\ttext-transform: uppercase;\n\tfont-weight: 300;\n\tcolor: #000000;\n\t-webkit-text-decoration-color: #000000;\n\t        text-decoration-color: #000000;\n}\n\nh1 {\n    font-size: 2em;\n    text-align: center;\n}\n\nh2 {\n\n\tfont-size: 1.3em;\n}\n\n#three-column {\n\toverflow: hidden;\n\tpadding: 10px 0px 50px 0px;\n}\n\n#three-column .tbox1 {\n\tfloat: left;\n\twidth: 384px;\n\tmargin-right: 20px;\n}\n\n#three-column .tbox2 {\n\tfloat: left;\n\twidth: 384px;\n}\n\n#three-column .tbox3 {\n\tfloat: right;\n\twidth: 384px;\n}\n\n.box-style {\n\tbackground: #FFFFFF;\n\ttext-align: center;\n\tborder-radius: 15px;\n\n\n}\n\n.button\n{\n\tdisplay: inline-block;\n\tmargin: 1em 0em;\n\tbackground: #000;\n\tpadding: 1em 2em;\n\tcolor: #FFFFFF;\n}\n"

/***/ }),

/***/ "./src/app/componentes/assistance/assistance.component.html":
/*!******************************************************************!*\
  !*** ./src/app/componentes/assistance/assistance.component.html ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"assistance-container\" style=\"text-align:center\">\n  <div id=\"logo\" class=\"container\">\n    <h1>Servicios</h1>\n  </div>\n  <div class=\"container\">\n    <div class=\"row\">\n      <div class=\"col-md-4\" *ngFor=\"let mensaje of mensajes ;let i=index\" style=\"margin-bottom: 20px\">\n        <div class=\"card\" style=\"width: 350px; height: 350;\">\n          <div class=\"card-body\" style=\"width: 350px; height: 350;\">\n            <!--    <img src=\"images/help.png\" width=\"\" height=\"\" alt=\"\" />\n        <h2 class=\"card-title\"> Pasillo: {{mensajes.identificador}}</h2>\n        <h2 class=\"card-text\">Distancia: {{mensajes.distancia}}</h2>\n        <a href=\"#\" class=\"btn btn-primary\"> Atender solicitud </a>-->\n            <div class=\"tbox1\">\n              <div class=\"box-style box-style01\">\n                <div class=\"content\">\n                  <div class=\"image\"><img src=\"../assets/help.png\" /></div>\n                  <h2>Pasillo: {{mensaje.name}}</h2>\n                  <h2>Estado: {{mensaje.status}}</h2>\n                  <!--  <h2>Atiende: {{mensaje.help}}</h2>-->\n                  <!--<h2>id: {{mensaje.id}}</h2>-->\n                  <button class=\"button\" (click)=\"setResponse(mensaje.id, mensaje.status)\">Atender solicitud</button>\n                </div>\n              </div>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/componentes/assistance/assistance.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/componentes/assistance/assistance.component.ts ***!
  \****************************************************************/
/*! exports provided: AssistanceComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AssistanceComponent", function() { return AssistanceComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _firestore_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../firestore.service */ "./src/app/firestore.service.ts");
/* harmony import */ var _servicios_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../servicios/auth.service */ "./src/app/servicios/auth.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var angular_webstorage_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! angular-webstorage-service */ "./node_modules/angular-webstorage-service/bundles/angular-webstorage-service.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};






var AssistanceComponent = /** @class */ (function () {
    function AssistanceComponent(fireStoreService, authService, router, storage) {
        this.fireStoreService = fireStoreService;
        this.authService = authService;
        this.router = router;
        this.storage = storage;
        this.title = 'app';
        this.mensajes = [];
        this.getMensajes();
    }
    AssistanceComponent.prototype.ngOnInit = function () {
        var _this = this;
        var isLogin = this.storage.get("IS_LOGIN") || false;
        if (!isLogin) {
            this.router.navigate(['/']);
        }
        else {
            this.authService.getAuth().subscribe(function (auth) {
                if (auth) {
                    _this.nombre = auth.displayName;
                    _this.fotoUsuario = auth.photoURL;
                }
            });
        }
    };
    AssistanceComponent.prototype.getMensajes = function () {
        var _this = this;
        this.fireStoreService.getMensajes().onSnapshot(function (response) {
            _this.mensajes = [];
            if (!response.empty) {
                response.docs.forEach(function (query) {
                    _this.fireStoreService.getSections().doc(query.data().section.id).get().then(function (response) {
                        var mensaje = {
                            name: response.data().name,
                            status: query.data().status,
                            id: query.id
                        };
                        _this.mensajes.push(mensaje);
                    });
                });
            }
        });
    };
    AssistanceComponent.prototype.setResponse = function (id, status) {
        var _this = this;
        this.fireStoreService.setResponse(id, status, this.nombre, this.fotoUsuario).then(function (response) {
            _this.getMensajes();
        });
    };
    AssistanceComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-assistance',
            template: __webpack_require__(/*! ./assistance.component.html */ "./src/app/componentes/assistance/assistance.component.html"),
            styles: [__webpack_require__(/*! ./assistance.component.css */ "./src/app/componentes/assistance/assistance.component.css")]
        }),
        __param(3, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(angular_webstorage_service__WEBPACK_IMPORTED_MODULE_4__["SESSION_STORAGE"])),
        __metadata("design:paramtypes", [_firestore_service__WEBPACK_IMPORTED_MODULE_1__["FireStoreService"], _servicios_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], Object])
    ], AssistanceComponent);
    return AssistanceComponent;
}());



/***/ }),

/***/ "./src/app/componentes/estadisticas/estadisticas.component.css":
/*!*********************************************************************!*\
  !*** ./src/app/componentes/estadisticas/estadisticas.component.css ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/componentes/estadisticas/estadisticas.component.html":
/*!**********************************************************************!*\
  !*** ./src/app/componentes/estadisticas/estadisticas.component.html ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\n  estadisticas works!\n</p>\n"

/***/ }),

/***/ "./src/app/componentes/estadisticas/estadisticas.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/componentes/estadisticas/estadisticas.component.ts ***!
  \********************************************************************/
/*! exports provided: EstadisticasComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EstadisticasComponent", function() { return EstadisticasComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var angular_webstorage_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! angular-webstorage-service */ "./node_modules/angular-webstorage-service/bundles/angular-webstorage-service.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};




var EstadisticasComponent = /** @class */ (function () {
    function EstadisticasComponent(router, storage) {
        this.router = router;
        this.storage = storage;
    }
    EstadisticasComponent.prototype.ngOnInit = function () {
        var isLogin = this.storage.get("IS_LOGIN") || false;
        if (!isLogin) {
            this.router.navigate(['/']);
        }
    };
    EstadisticasComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-estadisticas',
            template: __webpack_require__(/*! ./estadisticas.component.html */ "./src/app/componentes/estadisticas/estadisticas.component.html"),
            styles: [__webpack_require__(/*! ./estadisticas.component.css */ "./src/app/componentes/estadisticas/estadisticas.component.css")]
        }),
        __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(angular_webstorage_service__WEBPACK_IMPORTED_MODULE_2__["SESSION_STORAGE"])),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], Object])
    ], EstadisticasComponent);
    return EstadisticasComponent;
}());



/***/ }),

/***/ "./src/app/componentes/home-page/home-page.component.css":
/*!***************************************************************!*\
  !*** ./src/app/componentes/home-page/home-page.component.css ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".div-with-bg\n{\nbackground-image: url('https://www.seriouseats.com/images/2016/01/20160115-things-never-to-but-at-supermarket-.jpg') ;\n}\n.jumbotron \n{\n  height: 100vh;\n}\n.btn-google\n{\n    background: #dd4b39;\n    color:  #ffffff;\n}\n\n"

/***/ }),

/***/ "./src/app/componentes/home-page/home-page.component.html":
/*!****************************************************************!*\
  !*** ./src/app/componentes/home-page/home-page.component.html ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"div-with-bg\" >\n  <head>\n  <link rel=\"stylesheet\" href=\"https://use.fontawesome.com/releases/v5.5.0/css/all.css\" integrity=\"sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU\" crossorigin=\"anonymous\">\n</head>\n\n<div class=\"jumbotron\">\n  <h1 class=\"display-3\" style=\"text-align: center\"> Plataforma web de Eco </h1>\n  <p class=\"lead\" style=\"text-align: center\">Inicia sesion para ver más información.</p>\n  <hr class=\"my-4\">\n  <p class=\"lead\" style=\"text-align: center\">\n    <button class=\"btn btn-lg btn-google\" (click)=\"onClickGoogleLoging()\">\n      <i class=\"fab fa-google-plus-g\"></i>\n      Conectar con Google\n    </button>\n  </p>\n</div>\n</div>\n"

/***/ }),

/***/ "./src/app/componentes/home-page/home-page.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/componentes/home-page/home-page.component.ts ***!
  \**************************************************************/
/*! exports provided: HomePageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageComponent", function() { return HomePageComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _servicios_auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../servicios/auth.service */ "./src/app/servicios/auth.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var angular_webstorage_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! angular-webstorage-service */ "./node_modules/angular-webstorage-service/bundles/angular-webstorage-service.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};





var HomePageComponent = /** @class */ (function () {
    function HomePageComponent(authService, router, storage) {
        this.authService = authService;
        this.router = router;
        this.storage = storage;
    }
    HomePageComponent.prototype.ngOnInit = function () {
        var isLogin = this.storage.get("IS_LOGIN") || false;
        if (isLogin) {
            this.router.navigate(['/private']);
        }
    };
    HomePageComponent.prototype.onClickGoogleLoging = function () {
        var _this = this;
        this.authService.googleLogin().then(function (res) {
            _this.router.navigate(['/private']);
        }).catch(function (err) { return console.log(err.message); });
    };
    HomePageComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-home-page',
            template: __webpack_require__(/*! ./home-page.component.html */ "./src/app/componentes/home-page/home-page.component.html"),
            styles: [__webpack_require__(/*! ./home-page.component.css */ "./src/app/componentes/home-page/home-page.component.css")]
        }),
        __param(2, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(angular_webstorage_service__WEBPACK_IMPORTED_MODULE_3__["SESSION_STORAGE"])),
        __metadata("design:paramtypes", [_servicios_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], Object])
    ], HomePageComponent);
    return HomePageComponent;
}());



/***/ }),

/***/ "./src/app/componentes/login-page/login-page.component.css":
/*!*****************************************************************!*\
  !*** ./src/app/componentes/login-page/login-page.component.css ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/componentes/login-page/login-page.component.html":
/*!******************************************************************!*\
  !*** ./src/app/componentes/login-page/login-page.component.html ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div style=\"text-align: center\">\n<div class=\"row\">\n  <div class=\"col-md-6 mx-auto mt-5\">\n    <div class=\"card-body\">\n      <h1> Inicio de sesion</h1>\n      <form (submit) =\"onSubmitLogin()\">\n        <div class=\"form-group\">\n          <label for=\"email\">Email</label>\n          <input type=\"email\" name=\"email\" id=\"email\" [(ngModel)]=\"email\" class=\"form-control\">\n        </div>\n        <div class=\"form-group\">\n        <label for=\"password\">Contraseña</label>\n        <input type=\"password\" name=\"password\" id=\"password\" [(ngModel)]=\"password\"class=\"form-control\">\n      </div>\n           <input type=\"submit\" value=\"enviar\" class=\"btn btn-primary btn-block btn-lg\">\n      <br>\n      <br>\n      <a  class=\"btn btn-primary \" href=\"register\" role=\"button\" >Registrarse </a>\n      \n      </form>\n    </div>\n   </div>\n  </div>\n</div>\n\n"

/***/ }),

/***/ "./src/app/componentes/login-page/login-page.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/componentes/login-page/login-page.component.ts ***!
  \****************************************************************/
/*! exports provided: LoginPageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageComponent", function() { return LoginPageComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _servicios_auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../servicios/auth.service */ "./src/app/servicios/auth.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var LoginPageComponent = /** @class */ (function () {
    function LoginPageComponent(authSevice, router) {
        this.authSevice = authSevice;
        this.router = router;
    }
    LoginPageComponent.prototype.ngOnInit = function () {
    };
    LoginPageComponent.prototype.onSubmitLogin = function () {
        var _this = this;
        this.authSevice.logIn(this.email, this.password)
            .then(function (res) {
            _this.router.navigate(['/private']);
        }).catch(function (err) {
            console.log(err);
            _this.router.navigate(['/register']);
        });
    };
    LoginPageComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-login-page',
            template: __webpack_require__(/*! ./login-page.component.html */ "./src/app/componentes/login-page/login-page.component.html"),
            styles: [__webpack_require__(/*! ./login-page.component.css */ "./src/app/componentes/login-page/login-page.component.css")]
        }),
        __metadata("design:paramtypes", [_servicios_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], LoginPageComponent);
    return LoginPageComponent;
}());



/***/ }),

/***/ "./src/app/componentes/mapeo/mapeo.component.css":
/*!*******************************************************!*\
  !*** ./src/app/componentes/mapeo/mapeo.component.css ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".supermarket_container, .add_product_container {\n  padding: 20px 10px;\n}\n\n.btn_see_detail_supermarket {\n  margin-left: 10px;\n}\n"

/***/ }),

/***/ "./src/app/componentes/mapeo/mapeo.component.html":
/*!********************************************************!*\
  !*** ./src/app/componentes/mapeo/mapeo.component.html ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js\"></script>\n<script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\"></script>\n<div class=\"container\" style=\"text-align: center\">\n  <div class=\"supermarket_container\">\n    <h2>Supermercados</h2>\n    <table class=\"table table-hover\" style=\"text-align: center\">\n      <thead>\n        <tr>\n          <th>Supermercado</th>\n          <th>Telefono</th>\n          <th>Latitud</th>\n          <th>Longitud</th>\n          <th>Opciones</th>\n        </tr>\n      </thead>\n      <tbody>\n        <tr>\n          <td>\n            <input [(ngModel)]=\"new_supermarket_name\">\n          </td>\n          <td>\n            <input [(ngModel)]=\"new_supermarket_phone\" type=\"number\">\n          </td>\n          <td>\n            <input [(ngModel)]=\"new_supermarket_lat\" type=\"number\" step=\"0.01\">\n          </td>\n          <td>\n            <input [(ngModel)]=\"new_supermarket_lon\" type=\"number\" step=\"0.01\">\n          </td>\n          <td>\n            <button class=\"btn btn-sm btn-primary\" style=\"text-align:center\" (click)=\"addSupermercado()\">Agregar</button>\n          </td>\n        </tr>\n        <tr *ngFor=\"let supermarket of supermarkets\">\n          <td>{{supermarket.name}}</td>\n          <td>{{supermarket.phone}}</td>\n          <td>{{supermarket.location.latitude}}</td>\n          <td>{{supermarket.location.longitude}}</td>\n          <td>\n            <a href=\"{{supermarket.google_maps_link}}\" target=\"_blank\"  class=\"btn btn-sm btn-primary\">Ver en Google Maps</a>\n            <button class=\"btn btn-sm btn-primary btn_see_detail_supermarket\" style=\"text-align:center\" (click)=\"goToDetail(supermarket)\">Ver Detalle</button>\n            <button class=\"btn btn-sm btn-primary btn_see_detail_supermarket\" style=\"text-align:center\" (click)=\"deleteSupermarket(supermarket)\">Eliminar</button>\n          </td>\n        </tr>\n      </tbody>\n    </table>\n  </div>\n  <div class=\"add_product_container\">\n    <h2>Agregar productos </h2>\n    <table class=\"table table-hover\" style=\"text-align: center\">\n      <thead>\n        <tr>\n          <th>Dato</th>\n          <th>Valor</th>\n        </tr>\n      </thead>\n      <tbody>\n        <tr>\n          <td>Nombre:</td>\n          <td>\n            <input #name [(ngModel)]=\"new_product_name\">\n          </td>\n        </tr>\n        <tr>\n          <td>Precio:</td>\n          <td>\n            <input type=\"number\" #price [(ngModel)]=\"new_product_price\">\n          </td>\n\n        </tr>\n        <tr>\n          <td> Moneda:</td>\n          <td>\n            <input  #iso [(ngModel)]=\"new_product_iso\">\n          </td>\n        </tr>\n\n        <tr>\n          <td> Descripcion español:</td>\n          <td>\n            <input #descripcion [(ngModel)]=\"new_product_descripcion\">\n          </td>\n        </tr>\n        <tr>\n          <td> Descripcion ingles:</td>\n          <td>\n            <input #description [(ngModel)]=\"new_product_description\">\n          </td>\n        </tr>\n        <tr>\n          <td> Imagen:</td>\n          <td>\n            <input #imagenUrl [(ngModel)]=\"new_product_image_url\">\n          </td>\n        </tr>\n        <tr>\n          <td> Supermercado:</td>\n          <td>\n            <select #idSupermercado [(ngModel)]=\"new_product_supermarket\">\n              <option *ngFor=\"let supermarket of supermarkets\" value=\"{{supermarket.id}}\"> {{supermarket.name}}</option>\n            </select>\n          </td>\n        </tr>\n      </tbody>\n    </table>\n    <button class=\"btn btn-md btn-primary\" style=\"text-align:center\" (click)=\"addProduct(name.value, price.value,iso.value,descripcion.value,description.value, imagenUrl.value,idSupermercado.value)\">Agregar producto</button>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/componentes/mapeo/mapeo.component.ts":
/*!******************************************************!*\
  !*** ./src/app/componentes/mapeo/mapeo.component.ts ***!
  \******************************************************/
/*! exports provided: MapeoComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MapeoComponent", function() { return MapeoComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var angular_webstorage_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! angular-webstorage-service */ "./node_modules/angular-webstorage-service/bundles/angular-webstorage-service.es5.js");
/* harmony import */ var _firestore_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../firestore.service */ "./src/app/firestore.service.ts");
/* harmony import */ var _servicios_auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../servicios/auth.service */ "./src/app/servicios/auth.service.ts");
/* harmony import */ var angularfire2_database__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! angularfire2/database */ "./node_modules/angularfire2/database/index.js");
/* harmony import */ var angularfire2_database__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(angularfire2_database__WEBPACK_IMPORTED_MODULE_5__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};







//import {Productos} from'../../componentes/Productos/productos.component';
var MapeoComponent = /** @class */ (function () {
    function MapeoComponent(router, storage, fireStoreService, authService, af) {
        this.router = router;
        this.storage = storage;
        this.fireStoreService = fireStoreService;
        this.authService = authService;
        this.af = af;
        this.supermarkets = [];
        this.new_product_name = null;
        this.new_product_iso = null;
        this.new_product_price = null;
        this.new_product_descripcion = null;
        this.new_product_description = null;
        this.new_product_image_url = null;
        this.new_supermarket_name = null;
        this.new_supermarket_phone = null;
        this.new_supermarket_lat = null;
        this.new_supermarket_lon = null;
        this.new_product_supermarket = null;
        this.getSupermarkets();
    }
    MapeoComponent.prototype.ngOnInit = function () {
        var isLogin = this.storage.get("IS_LOGIN") || false;
        if (!isLogin) {
            this.router.navigate(['/']);
        }
    };
    MapeoComponent.prototype.addSupermercado = function () {
        var _this = this;
        this.fireStoreService.addSupermarket(this.new_supermarket_name, this.new_supermarket_phone, this.new_supermarket_lat, this.new_supermarket_lon, function (doc) {
            _this.getSupermarkets();
            _this.clearFields();
        });
    };
    MapeoComponent.prototype.addProduct = function (name, dPrice, iso, descripcion, description, imagenUrl, idSupermercado) {
        this.fireStoreService.addProduct(name, parseInt(dPrice), iso, descripcion, description, imagenUrl, idSupermercado);
        this.clearFields();
    };
    MapeoComponent.prototype.deleteSupermarket = function (supermarket) {
        var _this = this;
        var result = confirm("¿Esta seguro que quiere eliminar el registro?");
        if (result) {
            this.fireStoreService.deleteSupermarket(supermarket.id, function () {
                _this.getSupermarkets();
                _this.clearFields();
            });
        }
    };
    MapeoComponent.prototype.clearFields = function () {
        this.new_product_name = null;
        this.new_product_iso = null;
        this.new_product_price = null;
        this.new_product_descripcion = null;
        this.new_product_description = null;
        this.new_product_image_url = null;
        this.new_product_supermarket = null;
        this.new_supermarket_name = null;
        this.new_supermarket_phone = null;
        this.new_supermarket_lat = null;
        this.new_supermarket_lon = null;
    };
    MapeoComponent.prototype.getSupermarkets = function () {
        var _this = this;
        this.fireStoreService.getSupermarkets().get().
            then(function (response) {
            _this.supermarkets = [];
            if (!response.empty) {
                response.docs.forEach(function (doc) {
                    var supermarket = doc.data();
                    supermarket.id = doc.id;
                    var lat = supermarket.location.latitude;
                    var lon = supermarket.location.longitude;
                    supermarket.google_maps_link = "https://www.google.com/maps/search/?api=1&query=" + lat + "," + lon;
                    _this.supermarkets.push(supermarket);
                });
            }
        });
    };
    MapeoComponent.prototype.goToDetail = function (supermarket) {
        this.router.navigate(['/supermaket_detail'], { queryParams: { supermarket_id: supermarket.id } });
    };
    MapeoComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-mapeo',
            template: __webpack_require__(/*! ./mapeo.component.html */ "./src/app/componentes/mapeo/mapeo.component.html"),
            styles: [__webpack_require__(/*! ./mapeo.component.css */ "./src/app/componentes/mapeo/mapeo.component.css")]
        }),
        __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(angular_webstorage_service__WEBPACK_IMPORTED_MODULE_2__["SESSION_STORAGE"])),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], Object, _firestore_service__WEBPACK_IMPORTED_MODULE_3__["FireStoreService"], _servicios_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"], angularfire2_database__WEBPACK_IMPORTED_MODULE_5__["AngularFireDatabase"]])
    ], MapeoComponent);
    return MapeoComponent;
}());



/***/ }),

/***/ "./src/app/componentes/navbar/navbar.component.css":
/*!*********************************************************!*\
  !*** ./src/app/componentes/navbar/navbar.component.css ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "img.rounded-circle\n{\nwidth: 35 px;\nheight: 35px;\n}"

/***/ }),

/***/ "./src/app/componentes/navbar/navbar.component.html":
/*!**********************************************************!*\
  !*** ./src/app/componentes/navbar/navbar.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nav class=\"navbar navbar-expand-lg navbar-dark bg-dark\">\n  <a class=\"navbar-brand\" href=\"#\">ECO </a>\n  <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarColor02\" aria-controls=\"navbarColor02\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">\n    <span class=\"navbar-toggler-icon\"></span>\n  </button>\n\n  <div class=\"collapse navbar-collapse\" id=\"navbarColor02\">\n    <ul class=\"navbar-nav mr-auto\">\n      <li class=\"nav-item active\">\n        <a class=\"nav-link\" routerLink=\"{{ isLogin ? '/private' : '/'}}\">Inicio <span class=\"sr-only\">(current)</span></a>\n      </li>\n      <li class=\"nav-item\" *ngIf=\"isLogin\">\n        <a class=\"nav-link\" href=\"#\" routerLink=\"/assistance\" >Asistencia</a>\n      </li>\n      <li class=\"nav-item\" *ngIf=\"isLogin\">\n        <a class=\"nav-link\" href=\"#\"routerLink=\"/estadistica\">Estadisticas</a>\n      </li>\n      <li class=\"nav-item\" *ngIf=\"isLogin\">\n        <a class=\"nav-link\" href=\"#\" routerLink=\"/mapeo\">Mapeo</a>\n      </li>\n    </ul>\n    <ul class=\"navbar-nav ml-auto\">\n      <li class=\"nav-item\" *ngIf=\"isLogin\">\n        <a class=\"nav-link\" href=\"#\" (click)=\"onClickLogout()\" >Salir</a>\n      </li>\n      <li class=\"nav-item\" *ngIf=\"isLogin\">\n          <a  class=\"nav-link\">{{nombre}}</a>\n      </li>\n      <li class=\"nav-item\" *ngIf=\"isLogin\">\n          <img src =\"{{fotoUsuario}}\"class=\"rounded-circle\">\n      </li>\n    </ul>\n  </div>\n</nav>\n"

/***/ }),

/***/ "./src/app/componentes/navbar/navbar.component.ts":
/*!********************************************************!*\
  !*** ./src/app/componentes/navbar/navbar.component.ts ***!
  \********************************************************/
/*! exports provided: NavbarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavbarComponent", function() { return NavbarComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _servicios_auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../servicios/auth.service */ "./src/app/servicios/auth.service.ts");
/* harmony import */ var angular_webstorage_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! angular-webstorage-service */ "./node_modules/angular-webstorage-service/bundles/angular-webstorage-service.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};




var NavbarComponent = /** @class */ (function () {
    function NavbarComponent(authService, storage) {
        this.authService = authService;
        this.storage = storage;
    }
    NavbarComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.authService.getAuth().subscribe(function (auth) {
            if (auth) {
                _this.isLogin = true;
                _this.storage.set("IS_LOGIN", true);
                _this.nombre = auth.displayName;
                _this.emailUsuario = auth.email;
                _this.fotoUsuario = auth.photoURL;
            }
            else {
                _this.storage.set("IS_LOGIN", false);
                _this.isLogin = false;
            }
        });
    };
    NavbarComponent.prototype.onClickLogout = function () {
        this.authService.logout();
        this.storage.set("IS_LOGIN", false);
    };
    NavbarComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-navbar',
            template: __webpack_require__(/*! ./navbar.component.html */ "./src/app/componentes/navbar/navbar.component.html"),
            styles: [__webpack_require__(/*! ./navbar.component.css */ "./src/app/componentes/navbar/navbar.component.css")]
        }),
        __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(angular_webstorage_service__WEBPACK_IMPORTED_MODULE_2__["SESSION_STORAGE"])),
        __metadata("design:paramtypes", [_servicios_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"], Object])
    ], NavbarComponent);
    return NavbarComponent;
}());



/***/ }),

/***/ "./src/app/componentes/not-found-page/not-found-page.component.css":
/*!*************************************************************************!*\
  !*** ./src/app/componentes/not-found-page/not-found-page.component.css ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/componentes/not-found-page/not-found-page.component.html":
/*!**************************************************************************!*\
  !*** ./src/app/componentes/not-found-page/not-found-page.component.html ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div >\n<h1 style=\"text-align:  center \"  >\n  Error, esta pg no existe!\n<br>\n<a href=\"/\" class=\"btn -prymary btn-lg\">Volver a la pagina principal</a>\n</h1>\n  </div>>"

/***/ }),

/***/ "./src/app/componentes/not-found-page/not-found-page.component.ts":
/*!************************************************************************!*\
  !*** ./src/app/componentes/not-found-page/not-found-page.component.ts ***!
  \************************************************************************/
/*! exports provided: NotFoundPageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotFoundPageComponent", function() { return NotFoundPageComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var NotFoundPageComponent = /** @class */ (function () {
    function NotFoundPageComponent() {
    }
    NotFoundPageComponent.prototype.ngOnInit = function () {
    };
    NotFoundPageComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-not-found-page',
            template: __webpack_require__(/*! ./not-found-page.component.html */ "./src/app/componentes/not-found-page/not-found-page.component.html"),
            styles: [__webpack_require__(/*! ./not-found-page.component.css */ "./src/app/componentes/not-found-page/not-found-page.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], NotFoundPageComponent);
    return NotFoundPageComponent;
}());



/***/ }),

/***/ "./src/app/componentes/privado-page/privado-page.component.css":
/*!*********************************************************************!*\
  !*** ./src/app/componentes/privado-page/privado-page.component.css ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".btn{\n    width: 32%;\n\n}\n\n.jumbotron {\n  height: 100vh;\n}\n\n.estilo\n{\nmargin-right: 10px;\n}"

/***/ }),

/***/ "./src/app/componentes/privado-page/privado-page.component.html":
/*!**********************************************************************!*\
  !*** ./src/app/componentes/privado-page/privado-page.component.html ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"jumbotron\">\n  <h1 class=\"display-3\" style=\"text-align: center\"> Plataforma web de eco </h1>\n  <p class=\"lead\" style=\"text-align: center\">Bienvenido tenemos estos servicios para ti.</p>\n  <div class=\"card\" style=\"text-align: center\">\n    <div class=\"row\">\n      <div class=\"col-md-2\"></div>\n      <div class=\"col-md-8\">\n        <div class=\"card-body\">\n          <button class=\"btn  btn-lg estilo\" href=\"assitance\" role=\"button\"> Asistencia </button>\n\n          <button class=\"btn  btn-lg estilo\" href=\"estadistica\" role=\"button\">Estadisticas </button>\n\n          <button class=\"btn  btn-lg estilo\" href=\"mapeo\" role=\"button\">Mapeo </button>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/componentes/privado-page/privado-page.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/componentes/privado-page/privado-page.component.ts ***!
  \********************************************************************/
/*! exports provided: PrivadoPageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PrivadoPageComponent", function() { return PrivadoPageComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var angular_webstorage_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! angular-webstorage-service */ "./node_modules/angular-webstorage-service/bundles/angular-webstorage-service.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};




var PrivadoPageComponent = /** @class */ (function () {
    function PrivadoPageComponent(router, storage) {
        this.router = router;
        this.storage = storage;
    }
    PrivadoPageComponent.prototype.ngOnInit = function () {
        var isLogin = this.storage.get("IS_LOGIN") || false;
        if (!isLogin) {
            this.router.navigate(['/']);
        }
    };
    PrivadoPageComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-privado-page',
            template: __webpack_require__(/*! ./privado-page.component.html */ "./src/app/componentes/privado-page/privado-page.component.html"),
            styles: [__webpack_require__(/*! ./privado-page.component.css */ "./src/app/componentes/privado-page/privado-page.component.css")]
        }),
        __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(angular_webstorage_service__WEBPACK_IMPORTED_MODULE_2__["SESSION_STORAGE"])),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], Object])
    ], PrivadoPageComponent);
    return PrivadoPageComponent;
}());



/***/ }),

/***/ "./src/app/componentes/register-page/register-page.component.css":
/*!***********************************************************************!*\
  !*** ./src/app/componentes/register-page/register-page.component.css ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".btn-google{\n    background: #dd4b39;\n    color:  #ffffff;\n}"

/***/ }),

/***/ "./src/app/componentes/register-page/register-page.component.html":
/*!************************************************************************!*\
  !*** ./src/app/componentes/register-page/register-page.component.html ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<head>\n  <link rel=\"stylesheet\" href=\"https://use.fontawesome.com/releases/v5.5.0/css/all.css\" integrity=\"sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU\" crossorigin=\"anonymous\">\n</head>\n\n\n<div style=\"text-align: center\">\n    <div class=\"row\">\n      <div class=\"col-md-6 mx-auto mt-5\">\n        <div class=\"card-body\">\n          <h1> Registrarse o iniciar sesion  </h1>\n          \n          <button class=\"btn btn-lg btn-google\" (click)=\"onClickGoogleLoging()\">\n              <i class=\"fab fa-google-plus-g\"></i>Conectar con Google  </button>\n\n        </div>\n       </div>\n      </div>\n       </div>\n\n    \n"

/***/ }),

/***/ "./src/app/componentes/register-page/register-page.component.ts":
/*!**********************************************************************!*\
  !*** ./src/app/componentes/register-page/register-page.component.ts ***!
  \**********************************************************************/
/*! exports provided: RegisterPageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterPageComponent", function() { return RegisterPageComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _servicios_auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../servicios/auth.service */ "./src/app/servicios/auth.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var RegisterPageComponent = /** @class */ (function () {
    function RegisterPageComponent(authService, router) {
        this.authService = authService;
        this.router = router;
    }
    RegisterPageComponent.prototype.ngOnInit = function () {
    };
    RegisterPageComponent.prototype.onSubmitAddUser = function () {
        var _this = this;
        this.authService.registerUser(this.email, this.password)
            .then(function (res) {
            console.log('Guardado');
            console.log(res);
            _this.router.navigate(['/private']);
        }).catch(function (err) {
            console.log(err);
        });
    };
    RegisterPageComponent.prototype.onClickGoogleLoging = function () {
        var _this = this;
        this.authService.googleLogin().then(function (res) {
            _this.router.navigate(['/private']);
        }).catch(function (err) { return console.log(err.message); });
    };
    RegisterPageComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-register-page',
            template: __webpack_require__(/*! ./register-page.component.html */ "./src/app/componentes/register-page/register-page.component.html"),
            styles: [__webpack_require__(/*! ./register-page.component.css */ "./src/app/componentes/register-page/register-page.component.css")]
        }),
        __metadata("design:paramtypes", [_servicios_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], RegisterPageComponent);
    return RegisterPageComponent;
}());



/***/ }),

/***/ "./src/app/componentes/supermarket-detail/supermarket-detail.component.css":
/*!*********************************************************************************!*\
  !*** ./src/app/componentes/supermarket-detail/supermarket-detail.component.css ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".sections_container, .edges_container, .products_container {\n  padding: 20px 10px;\n}\n"

/***/ }),

/***/ "./src/app/componentes/supermarket-detail/supermarket-detail.component.html":
/*!**********************************************************************************!*\
  !*** ./src/app/componentes/supermarket-detail/supermarket-detail.component.html ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js\"></script>\n<script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\"></script>\n<div class=\"container\" style=\"text-align: center\">\n  <div class=\"sections_container\">\n    <h2>Secciones</h2>\n    <table class=\"table table-hover\" style=\"text-align: center\">\n      <thead>\n        <tr>\n          <th>Imagen</th>\n          <th>Sección</th>\n          <th>Beacon UUID</th>\n          <th>Opciones</th>\n        </tr>\n      </thead>\n      <tbody>\n        <tr>\n          <td>\n            <input [(ngModel)]=\"new_section_url\">\n          </td>\n          <td>\n            <input [(ngModel)]=\"new_section_name\">\n          </td>\n          <td>\n            Generated\n          </td>\n          <td>\n            <button class=\"btn btn-sm btn-primary\" style=\"text-align:center\" (click)=\"addSection()\">Agregar</button>\n          </td>\n        </tr>\n        <tr *ngFor=\"let section of sections\">\n          <td>\n            <img src=\"{{section.imageUrl}}\" alt=\"\" width=40 height=40 class=\"rounded-circle\">\n          </td>\n          <td>{{section.name}}</td>\n          <td>{{section.beaconUuid}}</td>\n          <td>\n            <button class=\"btn btn-sm btn-primary\" style=\"text-align:center\" (click)=\"deleteSection(section)\">Eliminar</button>\n          </td>\n        </tr>\n      </tbody>\n    </table>\n  </div>\n  <div class=\"edges_container\">\n    <h2>Rutas</h2>\n    <table class=\"table table-hover\" style=\"text-align: center\">\n      <thead>\n        <tr>\n          <th>Desde</th>\n          <th>Hacia</th>\n          <th>Dirección</th>\n          <th>Bearing</th>\n          <th>Distancia</th>\n          <th>Opciones</th>\n        </tr>\n      </thead>\n      <tbody>\n        <tr>\n          <td>\n            <select [(ngModel)]=\"new_edge_from\">\n              <option *ngFor=\"let section of sections\" value=\"{{section.beaconUuid}}\">{{section.name}}</option>\n            </select>\n          </td>\n          <td>\n            <select [(ngModel)]=\"new_edge_to\">\n              <option *ngFor=\"let section of sections\" value=\"{{section.beaconUuid}}\">{{section.name}}</option>\n            </select>\n          </td>\n          <td>\n            <select [(ngModel)]=\"new_edge_direction\">\n              <option value=\"Forward\">Forward</option>\n              <option value=\"Left\">Left</option>\n              <option value=\"Right\">Right</option>\n              <option value=\"Backwards\">Backwards</option>\n            </select>\n          </td>\n          <td>\n            <input [(ngModel)]=\"new_edge_bearing\" type=\"number\">\n          </td>\n          <td>\n            <input [(ngModel)]=\"new_edge_distance\" type=\"number\">\n          </td>\n          <td>\n            <button class=\"btn btn-sm btn-primary\" style=\"text-align:center\" (click)=\"addEdge()\">Agregar</button>\n          </td>\n        </tr>\n        <tr *ngFor=\"let edge of edges\">\n          <td>{{uuidToSectionName(edge.from)}}</td>\n          <td>{{uuidToSectionName(edge.to)}}</td>\n          <td>{{edge.direction}}</td>\n          <td>{{edge.bearing}} {{bearingToStringDirection(edge.bearing)}}</td>\n          <td>{{edge.distance}} {{edge.unit}}</td>\n          <td>\n            <button class=\"btn btn-sm btn-primary\" style=\"text-align:center\" (click)=\"deleteEdge(edge)\">Eliminar</button>\n          </td>\n        </tr>\n      </tbody>\n    </table>\n  </div>\n  <div class=\"products_container\">\n    <h2>Productos</h2>\n    <table class=\"table table-hover\" style=\"text-align: center\">\n      <thead>\n        <tr>\n          <th>Imagen</th>\n          <th>Nombre</th>\n          <th>Precio</th>\n          <th>Moneda</th>\n          <th>Descripción Español</th>\n          <th>Descripción Ingles</th>\n          <th>Opciones</th>\n        </tr>\n      </thead>\n      <tbody>\n        <tr *ngFor=\"let product of products\">\n          <td>\n            <img src=\"{{product.imageUrl}}\" alt=\"\" width=40 height=40 class=\"rounded-circle\">\n          </td>\n          <td>{{product.name}}</td>\n          <td>{{product.price.units}}</td>\n          <td>{{product.price.iso}}</td>\n          <td>{{product.descripcion}}</td>\n          <td>{{product.description}}</td>\n          <td>\n            <button class=\"btn btn-sm btn-primary\" style=\"text-align:center\" (click)=\"deleteProduct(product)\">Eliminar</button>\n          </td>\n        </tr>\n      </tbody>\n    </table>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/componentes/supermarket-detail/supermarket-detail.component.ts":
/*!********************************************************************************!*\
  !*** ./src/app/componentes/supermarket-detail/supermarket-detail.component.ts ***!
  \********************************************************************************/
/*! exports provided: SupermarketDetailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SupermarketDetailComponent", function() { return SupermarketDetailComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var angular_webstorage_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! angular-webstorage-service */ "./node_modules/angular-webstorage-service/bundles/angular-webstorage-service.es5.js");
/* harmony import */ var _firestore_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../firestore.service */ "./src/app/firestore.service.ts");
/* harmony import */ var _servicios_auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../servicios/auth.service */ "./src/app/servicios/auth.service.ts");
/* harmony import */ var angularfire2_database__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! angularfire2/database */ "./node_modules/angularfire2/database/index.js");
/* harmony import */ var angularfire2_database__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(angularfire2_database__WEBPACK_IMPORTED_MODULE_5__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};







var SupermarketDetailComponent = /** @class */ (function () {
    function SupermarketDetailComponent(router, route, storage, fireStoreService, authService, af) {
        this.router = router;
        this.route = route;
        this.storage = storage;
        this.fireStoreService = fireStoreService;
        this.authService = authService;
        this.af = af;
        // Data
        this.supermarketId = null;
        this.sections = [];
        this.edges = [];
        this.products = [];
        // Inputs
        this.new_section_name = null;
        this.new_section_url = null;
        this.new_edge_from = null;
        this.new_edge_to = null;
        this.new_edge_direction = null;
        this.new_edge_bearing = null;
        this.new_edge_distance = null;
    }
    SupermarketDetailComponent.prototype.ngOnInit = function () {
        var _this = this;
        var isLogin = this.storage.get("IS_LOGIN") || false;
        if (!isLogin) {
            this.router.navigate(['/']);
        }
        else {
            this.route.queryParams.subscribe(function (params) {
                _this.supermarketId = params['supermarket_id'];
                if (!_this.supermarketId) {
                    _this.router.navigate(['/mapeo']);
                }
                else {
                    _this.getSections();
                    _this.getProducts();
                }
            });
        }
    };
    // Get functions
    SupermarketDetailComponent.prototype.getSections = function () {
        var _this = this;
        this.sections = [];
        this.fireStoreService.getSupermarketSections(this.supermarketId).get()
            .then(function (docs) {
            docs.forEach(function (doc) {
                var section = doc.data();
                section.id = doc.id;
                _this.sections.push(section);
            });
            _this.getEdges();
        }).catch(function (error) {
            console.error("error agregando", error);
        });
    };
    SupermarketDetailComponent.prototype.getEdges = function () {
        var _this = this;
        this.edges = [];
        this.fireStoreService.getSupermarketEdges(this.supermarketId).get()
            .then(function (docs) {
            docs.forEach(function (doc) {
                var edge = doc.data();
                edge.id = doc.id;
                _this.edges.push(edge);
            });
        }).catch(function (error) {
            console.error("error agregando", error);
        });
    };
    SupermarketDetailComponent.prototype.getProducts = function () {
        var _this = this;
        this.products = [];
        this.fireStoreService.getSupermarketProducts(this.supermarketId).get()
            .then(function (docs) {
            docs.forEach(function (doc) {
                var product = doc.data();
                product.id = doc.id;
                _this.products.push(product);
            });
        }).catch(function (error) {
            console.error("error agregando", error);
        });
    };
    // Add functions
    SupermarketDetailComponent.prototype.addSection = function () {
        var _this = this;
        this.fireStoreService.addSections(this.supermarketId, {
            name: this.new_section_name,
            imageUrl: this.new_section_url,
            beaconUuid: this.generateUuid()
        }, function (doc) {
            _this.getSections();
            _this.clearFields();
        });
    };
    SupermarketDetailComponent.prototype.addEdge = function () {
        var _this = this;
        if (this.new_edge_from === this.new_edge_to) {
            alert("El origen y destino no pueden ser iguales");
        }
        else {
            this.fireStoreService.addEdge(this.supermarketId, {
                bearing: this.new_edge_bearing,
                direction: this.new_edge_direction,
                distance: this.new_edge_distance,
                from: this.new_edge_from,
                to: this.new_edge_to
            }, function (doc) {
                _this.getEdges();
                _this.clearFields();
            });
        }
    };
    // Delete functions
    SupermarketDetailComponent.prototype.deleteSection = function (section) {
        var _this = this;
        var result = confirm("¿Esta seguro que quiere eliminar el registro?");
        if (result) {
            this.fireStoreService.deleteSection(section.id, function () {
                _this.getSections();
                _this.clearFields();
            });
        }
    };
    SupermarketDetailComponent.prototype.deleteEdge = function (edge) {
        var _this = this;
        var result = confirm("¿Esta seguro que quiere eliminar el registro?");
        if (result) {
            this.fireStoreService.deleteEdge(edge.id, function () {
                _this.getEdges();
                _this.clearFields();
            });
        }
    };
    SupermarketDetailComponent.prototype.deleteProduct = function (product) {
        var _this = this;
        var result = confirm("¿Esta seguro que quiere eliminar el registro?");
        if (result) {
            this.fireStoreService.deleteProduct(product.id, function () {
                _this.getProducts();
                _this.clearFields();
            });
        }
    };
    // Helpers
    SupermarketDetailComponent.prototype.clearFields = function () {
        this.new_section_name = null;
        this.new_section_url = null;
        this.new_edge_from = null;
        this.new_edge_to = null;
        this.new_edge_direction = null;
        this.new_edge_bearing = null;
        this.new_edge_distance = null;
    };
    SupermarketDetailComponent.prototype.generateUuid = function () {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16).toUpperCase();
        });
    };
    SupermarketDetailComponent.prototype.uuidToSectionName = function (uuid) {
        for (var i = 0; i < this.sections.length; i++) {
            var doc = this.sections[i];
            if (doc.beaconUuid.toUpperCase() === uuid.toUpperCase()) {
                return doc.name;
            }
            ;
        }
        return "";
    };
    SupermarketDetailComponent.prototype.bearingToStringDirection = function (bearing) {
        var step = 22.5;
        if (bearing >= 0 && bearing < step || bearing > 360 - step) {
            return "N";
        }
        else if (bearing >= step && bearing < step * 3) {
            return "NE";
        }
        else if (bearing >= step * 3 && bearing < step * 5) {
            return "E";
        }
        else if (bearing >= step * 5 && bearing < step * 7) {
            return "SE";
        }
        else if (bearing >= step * 7 && bearing < step * 9) {
            return "S";
        }
        else if (bearing >= step * 9 && bearing < step * 11) {
            return "SW";
        }
        else if (bearing >= step * 11 && bearing < step * 13) {
            return "W";
        }
        else if (bearing >= step * 13 && bearing < step * 15) {
            return "NW";
        }
        else {
            return "";
        }
    };
    SupermarketDetailComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-supermarket-detail',
            template: __webpack_require__(/*! ./supermarket-detail.component.html */ "./src/app/componentes/supermarket-detail/supermarket-detail.component.html"),
            styles: [__webpack_require__(/*! ./supermarket-detail.component.css */ "./src/app/componentes/supermarket-detail/supermarket-detail.component.css")]
        }),
        __param(2, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(angular_webstorage_service__WEBPACK_IMPORTED_MODULE_2__["SESSION_STORAGE"])),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"], Object, _firestore_service__WEBPACK_IMPORTED_MODULE_3__["FireStoreService"], _servicios_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"], angularfire2_database__WEBPACK_IMPORTED_MODULE_5__["AngularFireDatabase"]])
    ], SupermarketDetailComponent);
    return SupermarketDetailComponent;
}());



/***/ }),

/***/ "./src/app/componentes/usuario/usuario.component.css":
/*!***********************************************************!*\
  !*** ./src/app/componentes/usuario/usuario.component.css ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/componentes/usuario/usuario.component.html":
/*!************************************************************!*\
  !*** ./src/app/componentes/usuario/usuario.component.html ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\n  usuario works!\n</p>\n"

/***/ }),

/***/ "./src/app/componentes/usuario/usuario.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/componentes/usuario/usuario.component.ts ***!
  \**********************************************************/
/*! exports provided: UsuarioComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsuarioComponent", function() { return UsuarioComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var UsuarioComponent = /** @class */ (function () {
    function UsuarioComponent() {
    }
    UsuarioComponent.prototype.ngOnInit = function () {
    };
    UsuarioComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-usuario',
            template: __webpack_require__(/*! ./usuario.component.html */ "./src/app/componentes/usuario/usuario.component.html"),
            styles: [__webpack_require__(/*! ./usuario.component.css */ "./src/app/componentes/usuario/usuario.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], UsuarioComponent);
    return UsuarioComponent;
}());



/***/ }),

/***/ "./src/app/firestore.service.ts":
/*!**************************************!*\
  !*** ./src/app/firestore.service.ts ***!
  \**************************************/
/*! exports provided: FireStoreService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FireStoreService", function() { return FireStoreService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/index.js");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! firebase/app */ "./node_modules/firebase/app/dist/index.cjs.js");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(firebase_app__WEBPACK_IMPORTED_MODULE_2__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var FireStoreService = /** @class */ (function () {
    function FireStoreService(database) {
        this.database = database;
        this.collectionMensajes = this.database.firestore.collection('messages');
        this.collectionSections = this.database.firestore.collection('sections');
        this.collectionProductos = this.database.firestore.collection('products');
        this.collectionSuperMercados = this.database.firestore.collection('supermarkets');
        this.collectionEdges = this.database.firestore.collection('supermarket_edges');
    }
    FireStoreService.prototype.getMensajes = function () {
        return this.collectionMensajes;
    };
    FireStoreService.prototype.getSections = function () {
        return this.collectionSections;
    };
    FireStoreService.prototype.setResponse = function (id, status, nombre, fotoUsuario) {
        //en doc va el ID de la solicitud que respondo
        if (status == 0) {
            return this.collectionMensajes.doc(id).update({ status: 1, atiende: nombre, foto: fotoUsuario });
        }
        else if (status == 1) {
            return this.collectionMensajes.doc(id).update({ status: 2 });
        }
        else {
            return this.collectionMensajes.doc(id).update({ status: -1 });
        }
    };
    FireStoreService.prototype.addSupermarket = function (name, phone, lat, lon, onAddedCallback) {
        this.collectionSuperMercados.add({
            name: name,
            phone: phone,
            location: new firebase_app__WEBPACK_IMPORTED_MODULE_2__["firestore"].GeoPoint(lat, lon),
            shouldUseDirection: false
        }).then(function (docRef) {
            onAddedCallback(docRef);
        }).catch(function (error) {
            console.error("error agregando", error);
        });
    };
    FireStoreService.prototype.addProduct = function (name, dPrice, iso, descripcion, description, imagenUrl, idSupermercado) {
        this.price = dPrice;
        this.collectionProductos.add({
            name: name,
            price: {
                iso: iso,
                units: this.price
            },
            descripcion: descripcion,
            description: description,
            imageUrl: imagenUrl,
            supermarkets: [
                this.collectionSuperMercados.doc(idSupermercado)
            ]
        }).then(function (docRef) {
            console.log("Se pudo escribir");
        }).catch(function (error) {
            console.error("error agregando", error);
        });
    };
    FireStoreService.prototype.getSupermarkets = function () {
        return this.collectionSuperMercados;
    };
    FireStoreService.prototype.getSupermarketSections = function (supermarketId) {
        return this.collectionSections.where("supermarket", "==", this.collectionSuperMercados.doc(supermarketId));
    };
    FireStoreService.prototype.getSupermarketEdges = function (supermarketId) {
        return this.collectionEdges.where("supermarket", "==", this.collectionSuperMercados.doc(supermarketId));
    };
    FireStoreService.prototype.getSupermarketProducts = function (supermarketId) {
        return this.collectionProductos.where("supermarkets", "array-contains", this.collectionSuperMercados.doc(supermarketId));
    };
    FireStoreService.prototype.addSections = function (supermarketId, newSection, onAddedCallback) {
        this.collectionSections.add({
            name: newSection.name,
            imageUrl: newSection.imageUrl,
            beaconUuid: newSection.beaconUuid,
            supermarket: this.collectionSuperMercados.doc(supermarketId)
        }).then(function (docRef) {
            onAddedCallback(docRef);
        }).catch(function (error) {
            console.error("error agregando", error);
        });
    };
    FireStoreService.prototype.addEdge = function (supermarketId, newEdge, onAddedCallback) {
        this.collectionEdges.add({
            bearing: newEdge.bearing,
            direction: newEdge.direction,
            distance: newEdge.distance,
            from: newEdge.from,
            to: newEdge.to,
            unit: "steps",
            supermarket: this.collectionSuperMercados.doc(supermarketId)
        }).then(function (docRef) {
            onAddedCallback(docRef);
        }).catch(function (error) {
            console.error("error agregando", error);
        });
    };
    FireStoreService.prototype.deleteSection = function (sectionId, onAddedCallback) {
        this.collectionSections.doc(sectionId).delete()
            .then(function () {
            onAddedCallback();
        }).catch(function (error) {
            console.error("error agregando", error);
        });
    };
    FireStoreService.prototype.deleteEdge = function (edgeId, onAddedCallback) {
        this.collectionEdges.doc(edgeId).delete()
            .then(function () {
            onAddedCallback();
        }).catch(function (error) {
            console.error("error agregando", error);
        });
    };
    FireStoreService.prototype.deleteProduct = function (productId, onAddedCallback) {
        this.collectionProductos.doc(productId).delete()
            .then(function () {
            onAddedCallback();
        }).catch(function (error) {
            console.error("error agregando", error);
        });
    };
    FireStoreService.prototype.deleteSupermarket = function (supermarketId, onAddedCallback) {
        this.collectionSuperMercados.doc(supermarketId).delete()
            .then(function () {
            onAddedCallback();
        }).catch(function (error) {
            console.error("error agregando", error);
        });
    };
    FireStoreService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_fire_firestore__WEBPACK_IMPORTED_MODULE_1__["AngularFirestore"]])
    ], FireStoreService);
    return FireStoreService;
}());



/***/ }),

/***/ "./src/app/servicios/auth.service.ts":
/*!*******************************************!*\
  !*** ./src/app/servicios/auth.service.ts ***!
  \*******************************************/
/*! exports provided: AuthService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthService", function() { return AuthService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var angularfire2_auth__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! angularfire2/auth */ "./node_modules/angularfire2/auth/index.js");
/* harmony import */ var angularfire2_auth__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(angularfire2_auth__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! firebase/app */ "./node_modules/firebase/app/dist/index.cjs.js");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(firebase_app__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/index.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



//import {map, catchError} from "rxjs/operators";





var AuthService = /** @class */ (function () {
    function AuthService(afAuth, afs, router) {
        var _this = this;
        this.afAuth = afAuth;
        this.afs = afs;
        this.router = router;
        /// Get auth data, then get firestore user document || null
        this.user = this.afAuth.authState.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["switchMap"])(function (user) {
            if (user) {
                return _this.afs.doc("users/" + user.uid).valueChanges();
            }
            else {
                return Object(rxjs__WEBPACK_IMPORTED_MODULE_4__["of"])(null);
            }
        }));
    }
    AuthService.prototype.googleLogin = function () {
        var _this = this;
        return this.afAuth.auth.signInWithPopup(new firebase_app__WEBPACK_IMPORTED_MODULE_2__["auth"].GoogleAuthProvider).then(function (credential) {
            _this.updateUserData(credential.user);
        });
    };
    AuthService.prototype.registerUser = function (email, password) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.afAuth.auth.createUserWithEmailAndPassword(email, password)
                .then(function (userData) { return resolve(userData); }, function (err) { return reject(err); });
        });
    };
    AuthService.prototype.logIn = function (email, password) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.afAuth.auth.signInWithEmailAndPassword(email, password)
                .then(function (userData) { return resolve(userData); }, function (err) { return reject(err); });
        });
    };
    // TODO: solucionar mapa 
    AuthService.prototype.getAuth = function () {
        return this.afAuth.authState.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(function (auth) { return auth; }));
    };
    AuthService.prototype.updateUserData = function (user) {
        // Sets user data to firestore on login
        var userRef = this.afs.doc("users/" + user.uid);
        var data = {
            uid: user.uid,
            email: user.email,
            displayName: user.displayName,
            photoURL: user.photoURL,
            telefono: user.phoneNumber
        };
        return userRef.set(data, { merge: true });
    };
    AuthService.prototype.logout = function () {
        return this.afAuth.auth.signOut();
    };
    AuthService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({ providedIn: 'root' }),
        __metadata("design:paramtypes", [angularfire2_auth__WEBPACK_IMPORTED_MODULE_1__["AngularFireAuth"], _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_3__["AngularFirestore"],
            _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"]])
    ], AuthService);
    return AuthService;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
var environment = {
    production: false,
    firebase: {
        apiKey: "AIzaSyCFZk7qwThZlnQFGugMSChvdll9s9urzNg",
        authDomain: "ecoserver-278bf.firebaseapp.com",
        databaseURL: "https://ecoserver-278bf.firebaseio.com",
        projectId: "ecoserver-278bf",
        storageBucket: "ecoserver-278bf.appspot.com",
        messagingSenderId: "456918204156"
    }
};


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Users/williamravelo/Desktop/isis3510_201810_team5web/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map