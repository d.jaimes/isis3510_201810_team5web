import { Component, OnInit } from '@angular/core';
import { FireStoreService } from '../../firestore.service';
import { AuthService } from '../../servicios/auth.service'
import { Inject, Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { SESSION_STORAGE, StorageService } from 'angular-webstorage-service';
import { element } from '@angular/core/src/render3/instructions';
import { TouchSequence } from 'selenium-webdriver';

@Component({
  selector: 'app-estadisticas',
  templateUrl: './estadisticas.component.html',
  styleUrls: ['./estadisticas.component.css']
})
export class EstadisticasComponent implements OnInit {

  TYPE_COLUMN_CHART = "ColumnChart";
  PIE_CHART = "PieChart";
  WIDTH = 1100;
  HEIGHT = 400;

  supermercados = [];

  // Secciones
  sections_id_hash = [];
  section_supermarket_filter = "todos"
  sections_graph = [];
  sections_data = [["",0]];
  columns = ['Section', 'Frecuencia'];

  // Productos
  products_id_hash = [];
  product_supermarket_filter = "todos";
  products_graph = [];
  products_data = [["",0]];
  productColumns = ['Producto', 'Frecuencia'];

  // Asistencias por supermercado
  assistance_section_id_hash = [];
  assistance_section_supermarket_filter = "todos";
  assistance_section_graph = [];
  assistance_section_data = [["",0]];
  assistanceSectionColumns = ['Seccion', 'Frecuencia'];

  // Supermercado por asistencia
  supermarket_assistance_id_hash = [];
  supermarket_assistance_filter = "todos";
  supermarket_assistance_graph = [];
  supermarket_assistance_data = [];
  supermarketAssistanceColumns = ['Supermercado', 'Frecuencia'];

  // Asistencia por empleado
  employ_assistance_id_hash = [];
  employ_assistance_filter = "todos";
  employ_assistance_graph = [];
  employ_assistance_data = [];
  employAssistanceColumns = ['Empleado', 'Frecuencia'];

  // Categorias mas preguntadas por supermercado
  supermarkets = [];
  typeCatMasPreg = "ColumnChart";
  myWidthCatMasPreg = 750;
  myHeightCatMasPreg = 400;

  // Productos mas preguntados
  supermercadosProductos = []
  typeProdMasPreg = "PieChart";

  // Supermercados mas visitados
  supermercadosVisitas = ['Supermercado', 'Visitas'];
  conteoVisitas = [];
  typeMasVisitas = "PieChart"

  constructor(private fireStoreService: FireStoreService, public authService: AuthService, public router: Router, @Inject(SESSION_STORAGE) private storage: StorageService) {
    this.getSupermarkets();
    this.getSections(this.getSectionSearch, undefined);
    this.getProducts(this.getProductsSearch, undefined);
    this.getAssistanceSections(this.getAssistanceSearch, undefined);
    this.getSupermarketAssistance(this.getSupermarketAssistanceSearch, undefined);
    this.getEmployAssistance(this.getEmployAssistanceSearch, undefined);
    this.contarCategoriasPorSupermercados();
    this.contarProductosPorSupermercado();
    this.contarVisitasSupermercados();
  }

  ngOnInit() {
    const isLogin = this.storage.get("IS_LOGIN") || false;

    if (!isLogin) {
      this.router.navigate(['/'])
    }
  }

  // ----------------------------------
  // Helpers
  // ----------------------------------
  isMorning(date) {
    return 0 <= date.getHours() && date.getHours() <= 12;
  }

  isNoon(date) {
    return 12 < date.getHours() && date.getHours() <= 17;
  }

  isNight(date) {
    return 17 < date.getHours() && date.getHours() <= 24;
  }

  containsSupermarket(supermarketId, supermarkets) {
    for(var i = 0; i < supermarkets.length; i++) {
      var supermarketReference = supermarkets[i];
      if(supermarketId === supermarketReference.id) return true;
    }
    return false;
  }

  getSupermarkets() {
    this.fireStoreService.getSupermarkets().get()
      .then(docs => {
        docs.forEach(doc => { 
          var supermarket = doc.data();
          supermarket.id = doc.id;
          this.supermarkets.push(supermarket);
        })
      }).catch(function (error) {
        console.log("Error getting documents: ", error);
      });
  }

  // ----------------------------------
  // Primera grafica
  // ----------------------------------
  getSectionSearch(bindor, filter) {
    bindor.sections_graph = [];
    bindor.sections_data = [["",0]];
    bindor.fireStoreService.getSectionSearch().get()
      .then(docs => {
        var sections_hash = {};
        docs.forEach(doc => {
          var section_search = doc.data();
          section_search.id = doc.id;
          var date = section_search.createdAt.toDate()
          var supermarketId = bindor.sections_id_hash[section_search.section.id].supermarket.id;
          if (((filter && filter(date)) || !filter) && (supermarketId === bindor.section_supermarket_filter || bindor.section_supermarket_filter === "todos")) {
            if (sections_hash[section_search.section.id] != undefined) {
              sections_hash[section_search.section.id] = sections_hash[section_search.section.id] + 1;
            } else {
              sections_hash[section_search.section.id] = 1
            }
          }
        });
        var keys = Object.keys(sections_hash);
        keys.sort(function (a, b) {
          return sections_hash[b] - sections_hash[a]
        });
        keys.forEach(function (section_h) {
          var name = bindor.sections_id_hash[section_h].name
          bindor.sections_data.push([name, sections_hash[section_h]]);
        });
        bindor.sections_graph.push({
          name: 'Secciones',
          count: bindor.sections_data
        });
      }).catch(function (error) {
        console.log("Error getting documents: ", error);
      });
  }

  getSections(onFinishCallback, filter) {
    this.fireStoreService.getSections().get()
      .then((docs) => {
        docs.forEach(doc => {
          var section = doc.data();
          section.id = doc.id;
          this.sections_id_hash[section.id] = section
        })
        onFinishCallback(this, filter);
      }).catch(function (error) {
        console.log("Error getting documents: ", error);
      });
  }

  getSectionsByFilter(filter) {
    if (filter === 'morning') {
      this.getSections(this.getSectionSearch, this.isMorning);
    } else if (filter === 'noon') {
      this.getSections(this.getSectionSearch, this.isNoon);
    } else if (filter === 'night') {
      this.getSections(this.getSectionSearch, this.isNight);
    } else {
      this.getSections(this.getSectionSearch, undefined);
    }
  }

  // ----------------------------------
  // Segunda grafica
  // ----------------------------------
  getProductsSearch(bindor, filter) {
    bindor.products_graph = [];
    bindor.products_data = [["",0]];
    bindor.fireStoreService.getProductSearch().get()
      .then(docs => {
        var products_hash = {};
        docs.forEach(doc => {
          var product_search = doc.data();
          product_search.id = doc.id;
          var date = product_search.createdAt.toDate()
          var supermarkets = bindor.products_id_hash[product_search.product.id].supermarkets;
          var containsFilter = bindor.containsSupermarket(bindor.product_supermarket_filter, supermarkets)
          if (((filter && filter(date)) || !filter) && (containsFilter || bindor.product_supermarket_filter === "todos")) {
            if (products_hash[product_search.product.id] != undefined) {
              products_hash[product_search.product.id] = products_hash[product_search.product.id] + 1;
            } else {
              products_hash[product_search.product.id] = 1
            }
          }
        });
        var keys = Object.keys(products_hash);
        keys.sort(function (a, b) {
          return products_hash[b] - products_hash[a]
        });
        keys.forEach(function (product_h) {
          var name = bindor.products_id_hash[product_h].name
          bindor.products_data.push([name, products_hash[product_h]]);
        });
        bindor.products_graph.push({
          name: 'Productos',
          count: bindor.products_data
        });
      }).catch(function (error) {
        console.log("Error getting documents: ", error);
      });
  }

  getProducts(onFinishCallback, filter) {
    this.fireStoreService.getProducts().get()
      .then((docs) => {
        docs.forEach(doc => {
          var product = doc.data();
          product.id = doc.id;
          this.products_id_hash[product.id] = product
        })
        onFinishCallback(this, filter);
      }).catch(function (error) {
        console.log("Error getting documents: ", error);
      });
  }

  getProductsByFilter(filter) {
    if (filter === 'morning') {
      this.getProducts(this.getProductsSearch, this.isMorning);
    } else if (filter === 'noon') {
      this.getProducts(this.getProductsSearch, this.isNoon);
    } else if (filter === 'night') {
      this.getProducts(this.getProductsSearch, this.isNight);
    } else {
      this.getProducts(this.getProductsSearch, undefined);
    }
  }

  // ----------------------------------
  // Tercera grafica
  // ----------------------------------
  
  getAssistanceSearch(bindor, filter) {
    bindor.assistance_section_graph = [];
    bindor.assistance_section_data = [["",0]];
    bindor.fireStoreService.getMensajes().get()
      .then(docs => {
        var assistance_section_hash = {};
        docs.forEach(doc => {
          var assistance_section_search = doc.data();
          assistance_section_search.id = doc.id;
          var date = assistance_section_search.createdAt.toDate()
          var supermarketId = bindor.assistance_section_id_hash[assistance_section_search.section.id].supermarket.id;
          if (((filter && filter(date)) || !filter) && (supermarketId === bindor.assistance_section_supermarket_filter || bindor.assistance_section_supermarket_filter === "todos")) {
            if (assistance_section_hash[assistance_section_search.section.id] != undefined) {
              assistance_section_hash[assistance_section_search.section.id] = assistance_section_hash[assistance_section_search.section.id] + 1;
            } else {
              assistance_section_hash[assistance_section_search.section.id] = 1
            }
          }
        });
        var keys = Object.keys(assistance_section_hash);
        keys.sort(function (a, b) {
          return assistance_section_hash[b] - assistance_section_hash[a]
        });
        keys.forEach(function (section_h) {
          var name = bindor.assistance_section_id_hash[section_h].name
          bindor.assistance_section_data.push([name, assistance_section_hash[section_h]]);
        });
        bindor.assistance_section_graph.push({
          name: 'Asistencia',
          count: bindor.assistance_section_data
        });
      }).catch(function (error) {
        console.log("Error getting documents: ", error);
      });
  }
  
  getAssistanceSections(onFinishCallback, filter) {
    this.fireStoreService.getSections().get()
      .then((docs) => {
        docs.forEach(doc => {
          var section = doc.data();
          section.id = doc.id;
          this.assistance_section_id_hash[section.id] = section
        })
        onFinishCallback(this, filter);
      }).catch(function (error) {
        console.log("Error getting documents: ", error);
      });
  }

  getAssistanceSupermarketByFilter(filter) {
    if (filter === 'morning') {
      this.getAssistanceSections(this.getAssistanceSearch, this.isMorning);
    } else if (filter === 'noon') {
      this.getAssistanceSections(this.getAssistanceSearch, this.isNoon);
    } else if (filter === 'night') {
      this.getAssistanceSections(this.getAssistanceSearch, this.isNight);
    } else {
      this.getAssistanceSections(this.getAssistanceSearch, undefined);
    }
  }

  // ----------------------------------
  // Cuarta grafica
  // ----------------------------------
  getSupermarketAssistanceSearch(bindor, filter) {
    bindor.supermarket_assistance_graph = [];
    bindor.supermarket_assistance_data = [];
    bindor.fireStoreService.getMensajes().get()
      .then(docs => {
        var supermarket_assistance_hash = {};
        docs.forEach(doc => {
          var supermarket_assistance_search = doc.data();
          supermarket_assistance_search.id = doc.id;
          var section = supermarket_assistance_search.section;
          var date = supermarket_assistance_search.createdAt.toDate();
          var supermarketId = bindor.assistance_section_id_hash[section.id].supermarket.id;
          if (((filter && filter(date)) || !filter)) {
            if (supermarket_assistance_hash[supermarketId] != undefined) {
              supermarket_assistance_hash[supermarketId] = supermarket_assistance_hash[supermarketId] + 1;
            } else {
              supermarket_assistance_hash[supermarketId] = 1
            }
          }
        });
        var keys = Object.keys(supermarket_assistance_hash);
        keys.sort(function (a, b) {
          return supermarket_assistance_hash[b] - supermarket_assistance_hash[a]
        });
        keys.forEach(function (section_h) {
          var name = bindor.supermarket_assistance_id_hash[section_h].name
          bindor.supermarket_assistance_data.push([name, supermarket_assistance_hash[section_h]]);
        });
        bindor.supermarket_assistance_graph.push({
          name: 'Asistencia',
          count: bindor.supermarket_assistance_data
        });
      }).catch(function (error) {
        console.log("Error getting documents: ", error);
      });
  }

  getSupermarketAssistance(onFinishCallback, filter) {
    this.fireStoreService.getSupermarkets().get()
      .then((docs) => {
        docs.forEach(doc => {
          var supermarket = doc.data();
          supermarket.id = doc.id;
          this.supermarket_assistance_id_hash[supermarket.id] = supermarket
        })
        onFinishCallback(this, filter);
      }).catch(function (error) {
        console.log("Error getting documents: ", error);
      });
  }

  getSupermarketAssistanceByFilter(filter) {
    if (filter === 'morning') {
      this.getSupermarketAssistance(this.getSupermarketAssistanceSearch, this.isMorning);
    } else if (filter === 'noon') {
      this.getSupermarketAssistance(this.getSupermarketAssistanceSearch, this.isNoon);
    } else if (filter === 'night') {
      this.getSupermarketAssistance(this.getSupermarketAssistanceSearch, this.isNight);
    } else {
      this.getSupermarketAssistance(this.getSupermarketAssistanceSearch, undefined);
    }
  }

  // ----------------------------------
  // Quinta grafica
  // ----------------------------------
  getEmployAssistanceSearch(bindor, filter) {
    bindor.employ_assistance_graph = [];
    bindor.employ_assistance_data = [];
    bindor.fireStoreService.getMensajes().get()
      .then(docs => {
        var employ_assistance_hash = {};
        docs.forEach(doc => {
          var employ_assistance_search = doc.data();
          employ_assistance_search.id = doc.id;
          var name = employ_assistance_search.atiende
          var date = employ_assistance_search.createdAt.toDate();
          if (((filter && filter(date)) || !filter)) {
            if (employ_assistance_hash[name] != undefined) {
              employ_assistance_hash[name] = employ_assistance_hash[name] + 1;
            } else if(name != undefined) {
              employ_assistance_hash[name] = 1
            }
          }
        });
        var keys = Object.keys(employ_assistance_hash);
        keys.sort(function (a, b) {
          return employ_assistance_hash[b] - employ_assistance_hash[a]
        });
        keys.forEach(function (name) {
          bindor.employ_assistance_data.push([name, employ_assistance_hash[name]]);
        });
        bindor.employ_assistance_graph.push({
          name: 'Asistencia',
          count: bindor.employ_assistance_data
        });
      }).catch(function (error) {
        console.log("Error getting documents: ", error);
      });
  }
 
  getEmployAssistance(onFinishCallback, filter) {
    this.fireStoreService.getUsers().get()
      .then((docs) => {
        docs.forEach(doc => {
          var user = doc.data();
          user.id = doc.id;
          this.employ_assistance_id_hash[user.id] = user
        })
        onFinishCallback(this, filter);
      }).catch(function (error) {
        console.log("Error getting documents: ", error);
      });
  }

  getEmployAssistanceByFilter(filter) {
    if (filter === 'morning') {
      this.getEmployAssistance(this.getEmployAssistanceSearch, this.isMorning);
    } else if (filter === 'noon') {
      this.getEmployAssistance(this.getEmployAssistanceSearch, this.isNoon);
    } else if (filter === 'night') {
      this.getEmployAssistance(this.getEmployAssistanceSearch, this.isNight);
    } else {
      this.getEmployAssistance(this.getEmployAssistanceSearch, undefined);
    }
  }

  // ----------------------------------
  // Primeras Graficas
  // ----------------------------------
  contarCategoriasPorSupermercados() {
    this.fireStoreService.getSupermarkets().get().
      then(response => {
        if (!response.empty) {
          response.docs.forEach(query => {
            this.fireStoreService.getSections().where("supermarket", "==", query.ref)
              .get().then(res => {
                var conteoSecciones = []
                var seccionActual = 0
                if (!res.empty) {
                  res.docs.forEach(que => {
                    this.fireStoreService.getSectionSearch().where("section", "==", que.ref)
                      .get().then((querySnapshot) => {
                        seccionActual = seccionActual + 1
                        conteoSecciones.push([que.data().name, querySnapshot.size])
                      }).then(() => {
                        if (seccionActual == res.docs.length) {
                          this.supermercados.push({
                            nombre: query.data().name,
                            conteoSecciones: conteoSecciones
                          })
                        }
                      })
                      .catch(function (error) {
                        console.log("Error getting documents: ", error);
                      });
                  })

                }
              });
          });
        }
      })
  }

  contarProductosPorSupermercado() {
    this.fireStoreService.getSupermarkets().get().
      then(response => {
        if (!response.empty) {
          response.docs.forEach(query => {
            this.fireStoreService.getProducts().where("supermarkets", "array-contains", query.ref)
              .get().then(res => {
                var conteoSecciones = []
                var productoActual = 0
                if (!res.empty) {
                  res.docs.forEach(que => {
                    this.fireStoreService.getProductSearch().where("product", "==", que.ref)
                      .get().then((querySnapshot) => {
                        productoActual = productoActual + 1
                        conteoSecciones.push([que.data().name, querySnapshot.size])
                      }).then(() => {
                        if (productoActual == res.docs.length) {
                          this.supermercadosProductos.push({
                            nombre: query.data().name,
                            conteoProductos: conteoSecciones
                          })
                        }
                      })
                      .catch(function (error) {
                        console.log("Error getting documents: ", error);
                      });
                  })
                }
              });
          });
        }
      })
  }

  contarVisitasSupermercados() {
    this.fireStoreService.getSupermarkets().get().
      then(response => {
        if (!response.empty) {
          response.docs.forEach((query) => {
            this.fireStoreService.getSupermarketCheckIns().where("supermarket", "==", query.ref)
              .get().then((res) => {
                this.conteoVisitas.push([query.data().name, res.size]);
              })
          });
        }
      })
  }
}



