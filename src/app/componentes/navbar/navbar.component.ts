import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../servicios/auth.service'
import { auditTime } from 'rxjs/internal/operators/auditTime';
import { auth } from 'firebase';
import { Inject, Injectable } from '@angular/core';
import { SESSION_STORAGE, StorageService } from 'angular-webstorage-service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  public isLogin: boolean;
  public nombre: string;
  public emailUsuario: string;
  public fotoUsuario: string;
  constructor(public authService: AuthService, @Inject(SESSION_STORAGE) private storage: StorageService) { }

  ngOnInit() {
    this.authService.getAuth().subscribe(auth => {
      if (auth) {
        this.isLogin = true;
        this.storage.set("IS_LOGIN", true);
        this.nombre = auth.displayName;
        this.emailUsuario = auth.email;
        this.fotoUsuario = auth.photoURL;
      }
      else {
        this.storage.set("IS_LOGIN", false);
        this.isLogin = false;
      }
    });
  }

  onClickLogout() {
    this.authService.logout();
    this.storage.set("IS_LOGIN", false);
  }

}
