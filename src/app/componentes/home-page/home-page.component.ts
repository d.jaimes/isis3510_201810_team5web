import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../servicios/auth.service'
import { from } from 'rxjs/internal/observable/from';
import { Router } from '@angular/router';
import { Inject, Injectable } from '@angular/core';
import { SESSION_STORAGE, StorageService } from 'angular-webstorage-service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {

  public email: string;
  public password: string;

  constructor(public authService: AuthService, public router: Router, @Inject(SESSION_STORAGE) private storage: StorageService) { }

  ngOnInit() {
    const isLogin = this.storage.get("IS_LOGIN") || false;
    if (isLogin) {
      this.router.navigate(['/private'])
    }
  }

  onClickGoogleLoging() {
    this.authService.googleLogin().then((res) => {
      this.router.navigate(['/private'])
    }).catch(err => console.log(err.message));
  }
}
