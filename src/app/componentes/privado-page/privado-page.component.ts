import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Inject, Injectable } from '@angular/core';
import { SESSION_STORAGE, StorageService } from 'angular-webstorage-service';

@Component({
  selector: 'app-privado-page',
  templateUrl: './privado-page.component.html',
  styleUrls: ['./privado-page.component.css']
})
export class PrivadoPageComponent implements OnInit {

  constructor(public router: Router, @Inject(SESSION_STORAGE) private storage: StorageService) { }

  ngOnInit() {
    const isLogin = this.storage.get("IS_LOGIN") || false;

    if(!isLogin) {
      this.router.navigate(['/'])
    }
  }

}
