import { Component, OnInit } from '@angular/core';
import { FireStoreService } from '../../firestore.service';
import { firestore } from 'firebase';
import { Query } from '@angular/core/src/metadata/di';
import { AuthService } from '../../servicios/auth.service'
import { auth } from 'firebase';
import { Inject, Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { SESSION_STORAGE, StorageService } from 'angular-webstorage-service';


@Component({
  selector: 'app-assistance',
  templateUrl: './assistance.component.html',
  styleUrls: ['./assistance.component.css']
})
export class AssistanceComponent implements OnInit {
  title = 'app';
  mensajes = [];
  public nombre: string;
  public fotoUsuario: string;

  constructor(private fireStoreService: FireStoreService, public authService: AuthService, public router: Router, @Inject(SESSION_STORAGE) private storage: StorageService) {
    this.getMensajes();
  }

  ngOnInit() {
    const isLogin = this.storage.get("IS_LOGIN") || false;

    if (!isLogin) {
      this.router.navigate(['/'])
    } else {
      this.authService.getAuth().subscribe(auth => {
        if (auth) {
          this.nombre = auth.displayName;
          this.fotoUsuario = auth.photoURL;
        }
      });
    }
  }

  getMensajes() {

    this.fireStoreService.getMensajes().onSnapshot(
      response => {
        this.mensajes = [];
        if (!response.empty) {
          response.docs.forEach(query => {
            this.fireStoreService.getSections().doc(query.data().section.id).get().then(response => {
              const mensaje = {
                name: response.data().name,
                status: query.data().status,
                id: query.id
              }
              this.mensajes.push(mensaje);
            });
          });
        }
      });
  }

  setResponse(id, status) {
    this.fireStoreService.setResponse(id, status, this.nombre, this.fotoUsuario).then(response => {
      this.getMensajes();
    });
  }
}
