import { Component, OnInit } from '@angular/core';
import{AuthService} from '../../servicios/auth.service';
import {Router} from '@angular/router';
@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent implements OnInit {
  public email:string;
  public password:string;

  constructor(public authSevice: AuthService, public router:Router) { }

  ngOnInit() {
  }
  onSubmitLogin()
  {
this.authSevice.logIn(this.email,this.password)
.then((res)=>{
this.router.navigate(['/private']);
}).catch((err)=>{
  console.log(err);
  this.router.navigate(['/register'])
})
  }
}
