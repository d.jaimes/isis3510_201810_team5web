import { Component, OnInit } from '@angular/core';
import { Inject, Injectable } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { SESSION_STORAGE, StorageService } from 'angular-webstorage-service';
import { AngularFireAuth } from 'angularfire2/auth'
import { FireStoreService } from '../../firestore.service';
import { Chart } from 'highcharts';
import { AuthService } from '../../servicios/auth.service'
import { Observable } from 'rxjs';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { Reference } from 'angularfire2/firestore';

@Component({
  selector: 'app-supermarket-detail',
  templateUrl: './supermarket-detail.component.html',
  styleUrls: ['./supermarket-detail.component.css']
})

export class SupermarketDetailComponent implements OnInit {

  // Data
  supermarketId = null;
  sections = [];
  edges = [];
  products = [];

  // Inputs
  new_section_name = null;
  new_section_url = null;

  new_edge_from = null
  new_edge_to = null
  new_edge_direction = null
  new_edge_bearing = null
  new_edge_distance = null

  constructor(public router: Router, public route: ActivatedRoute, @Inject(SESSION_STORAGE) private storage: StorageService, private fireStoreService: FireStoreService, public authService: AuthService, private af: AngularFireDatabase) {
  }

  ngOnInit() {
    const isLogin = this.storage.get("IS_LOGIN") || false;
    if (!isLogin) {
      this.router.navigate(['/'])
    } else {
      this.route.queryParams.subscribe(params => {
        this.supermarketId = params['supermarket_id'];
        if (!this.supermarketId) {
          this.router.navigate(['/mapeo'])
        } else {
          this.getSections();
          this.getProducts();
        }
      });
    }
  }

  // Get functions

  getSections() {
    this.sections = [];
    this.fireStoreService.getSupermarketSections(this.supermarketId).get()
      .then((docs) => {
        docs.forEach(doc => {
          var section = doc.data();
          section.id = doc.id;
          this.sections.push(section);
        });
        this.getEdges();
      }).catch(function(error) {
        console.error("error agregando", error);
      });
  }

  getEdges() {
    this.edges = [];
    this.fireStoreService.getSupermarketEdges(this.supermarketId).get()
      .then((docs) => {
        docs.forEach(doc => {
          var edge = doc.data();
          edge.id = doc.id;
          this.edges.push(edge);
        });
      }).catch(function(error) {
        console.error("error agregando", error);
      });
  }

  getProducts() {
    this.products = [];
    this.fireStoreService.getSupermarketProducts(this.supermarketId).get()
      .then((docs) => {
        docs.forEach(doc => {
          var product = doc.data();
          product.id = doc.id;
          this.products.push(product);
        });
      }).catch(function(error) {
        console.error("error agregando", error);
      });
  }

  // Add functions
  addSection() {
    this.fireStoreService.addSections(this.supermarketId, {
      name: this.new_section_name,
      imageUrl: this.new_section_url,
      beaconUuid: this.generateUuid()
    }, (doc) => {
      this.getSections();
      this.clearFields();
    });
  }

  addEdge() {
    if (this.new_edge_from === this.new_edge_to) {
      alert("El origen y destino no pueden ser iguales");
    } else {
      this.fireStoreService.addEdge(this.supermarketId, {
        bearing: this.new_edge_bearing,
        direction: this.new_edge_direction,
        distance: this.new_edge_distance,
        from: this.new_edge_from,
        to: this.new_edge_to
      }, (doc) => {
        this.getEdges();
        this.clearFields();
      });
    }
  }

  // Delete functions
  deleteSection(section) {
    var result = confirm("¿Esta seguro que quiere eliminar el registro?");
    if (result) {
      this.fireStoreService.deleteSection(section.id, () => {
        this.getSections();
        this.clearFields();
      });
    }
  }

  deleteEdge(edge) {
    var result = confirm("¿Esta seguro que quiere eliminar el registro?");
    if (result) {
      this.fireStoreService.deleteEdge(edge.id, () => {
        this.getEdges();
        this.clearFields();
      });
    }
  }

  deleteProduct(product) {
    var result = confirm("¿Esta seguro que quiere eliminar el registro?");
    if (result) {
      this.fireStoreService.deleteProduct(product.id, () => {
        this.getProducts();
        this.clearFields();
      });
    }
  }

  // Helpers
  clearFields() {
    this.new_section_name = null;
    this.new_section_url = null;

    this.new_edge_from = null;
    this.new_edge_to = null;
    this.new_edge_direction = null;
    this.new_edge_bearing = null;
    this.new_edge_distance = null;
  }

  generateUuid() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16).toUpperCase();
    });
  }

  uuidToSectionName(uuid) {
    for (var i = 0; i < this.sections.length; i++) {
      var doc = this.sections[i];
      if (doc.beaconUuid.toUpperCase() === uuid.toUpperCase()) {
        return doc.name;
      };
    }
    return "";
  }

  bearingToStringDirection(bearing) {
    var step = 22.5
    if (bearing >= 0 && bearing < step || bearing > 360 - step) {
      return "N"
    } else if (bearing >= step && bearing < step * 3) {
      return "NE"
    } else if (bearing >= step * 3 && bearing < step * 5) {
      return "E"
    } else if (bearing >= step * 5 && bearing < step * 7) {
      return "SE"
    } else if (bearing >= step * 7 && bearing < step * 9) {
      return "S"
    } else if (bearing >= step * 9 && bearing < step * 11) {
      return "SW"
    } else if (bearing >= step * 11 && bearing < step * 13) {
      return "W"
    } else if (bearing >= step * 13 && bearing < step * 15) {
      return "NW";
    } else {
      return "";
    }
  }
}
