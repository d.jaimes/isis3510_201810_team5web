import { Component, OnInit } from '@angular/core';
import { Inject, Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { SESSION_STORAGE, StorageService } from 'angular-webstorage-service';
import { AngularFireAuth } from 'angularfire2/auth'
import { FireStoreService } from '../../firestore.service';
import { Chart } from 'highcharts';
import { AuthService } from '../../servicios/auth.service'
import { Observable } from 'rxjs';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { Reference } from 'angularfire2/firestore';
//import {Productos} from'../../componentes/Productos/productos.component';

@Component({
  selector: 'app-mapeo',
  templateUrl: './mapeo.component.html',
  styleUrls: ['./mapeo.component.css']
})

export class MapeoComponent implements OnInit {

  supermarkets = [];
  new_product_name = null;
  new_product_iso = null;
  new_product_price = null;
  new_product_descripcion = null;
  new_product_description = null;
  new_product_image_url = null;
  new_supermarket_name = null;
  new_supermarket_phone = null;
  new_supermarket_lat = null;
  new_supermarket_lon = null;
  new_product_supermarket = null;

  constructor(public router: Router, @Inject(SESSION_STORAGE) private storage: StorageService, private fireStoreService: FireStoreService, public authService: AuthService, private af: AngularFireDatabase) {
    this.getSupermarkets();
  }

  ngOnInit() {
    const isLogin = this.storage.get("IS_LOGIN") || false;
    if (!isLogin) {
      this.router.navigate(['/'])
    }
  }

  addSupermercado() {
    this.fireStoreService.addSupermarket(this.new_supermarket_name, this.new_supermarket_phone, this.new_supermarket_lat, this.new_supermarket_lon, (doc) => {
      this.getSupermarkets();
      this.clearFields();
    });
  }

  addProduct(name: string, dPrice: string, iso: string, descripcion: string, description: string, imagenUrl: string, idSupermercado: string): void {
    this.fireStoreService.addProduct(name,
      parseInt(dPrice), iso, descripcion,
      description, imagenUrl, idSupermercado);
    this.clearFields();
  }

  deleteSupermarket(supermarket) {
    var result = confirm("¿Esta seguro que quiere eliminar el registro?");
    if (result) {
      this.fireStoreService.deleteSupermarket(supermarket.id, () => {
        this.getSupermarkets();
        this.clearFields();
      });
    }
  }

  clearFields() {
    this.new_product_name = null;
    this.new_product_iso = null;
    this.new_product_price = null;
    this.new_product_descripcion = null;
    this.new_product_description = null;
    this.new_product_image_url = null;
    this.new_product_supermarket = null;

    this.new_supermarket_name = null;
    this.new_supermarket_phone = null;
    this.new_supermarket_lat = null;
    this.new_supermarket_lon = null;
  }

  getSupermarkets() {
    this.fireStoreService.getSupermarkets().get().
      then(response => {
        this.supermarkets = [];
        if (!response.empty) {
          response.docs.forEach(doc => {
            let supermarket = doc.data();
            supermarket.id = doc.id;
            let lat = supermarket.location.latitude;
            let lon = supermarket.location.longitude;
            supermarket.google_maps_link = "https://www.google.com/maps/search/?api=1&query=" + lat + "," + lon;
            this.supermarkets.push(supermarket);
          });
        }
      });
  }

  goToDetail(supermarket) {
    this.router.navigate(['/supermaket_detail'], { queryParams: { supermarket_id: supermarket.id } });
  }
}
