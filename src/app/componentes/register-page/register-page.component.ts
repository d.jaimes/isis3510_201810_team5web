import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../servicios/auth.service'
import { from } from 'rxjs/internal/observable/from';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register-page',
  templateUrl: './register-page.component.html',
  styleUrls: ['./register-page.component.css']
})
export class RegisterPageComponent implements OnInit {

  public email: string;
  public password: string;
  constructor(public authService: AuthService, public router: Router) { }

  ngOnInit() {
  }

  onSubmitAddUser() {
    this.authService.registerUser(this.email, this.password)
      .then((res) => {
        console.log('Guardado');
        console.log(res);
        this.router.navigate(['/private']);

      }).catch((err) => {
        console.log(err);
      })
  }
  
  onClickGoogleLoging() {
    this.authService.googleLogin().then((res) => {
      this.router.navigate(['/private'])

    }).catch(err => console.log(err.message));

  }
}
