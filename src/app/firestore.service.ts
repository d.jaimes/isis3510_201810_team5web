import { Injectable } from '@angular/core';
import { AngularFirestore, CollectionReference } from '@angular/fire/firestore';
import { database } from 'firebase';
import * as firebase from 'firebase/app'

@Injectable()
export class FireStoreService {
    price: number;

    private collectionMensajes: CollectionReference;
    private collectionSections: CollectionReference;
    private collectionProductos: CollectionReference;
    private collectionSuperMercados: CollectionReference;
    private collectionSectionSearch: CollectionReference;
    private collectionProductSearch: CollectionReference;
    private collectionSupermarketCheckIns: CollectionReference;
    private collectionEdges: CollectionReference;
    private collectionUsers: CollectionReference;

    identificador;

    constructor(private database: AngularFirestore) {
        this.collectionMensajes = this.database.firestore.collection('messages');
        this.collectionSections = this.database.firestore.collection('sections');
        this.collectionProductos = this.database.firestore.collection('products');
        this.collectionSuperMercados = this.database.firestore.collection('supermarkets');
        this.collectionSectionSearch = this.database.firestore.collection('section_search');
        this.collectionProductSearch = this.database.firestore.collection('product_search');
        this.collectionSupermarketCheckIns = this.database.firestore.collection('supermarket_checkins');
        this.collectionEdges = this.database.firestore.collection('supermarket_edges');
        this.collectionUsers = this.database.firestore.collection('users');
    }

    getProductSearch() {
        return this.collectionProductSearch;
    }

    getSupermarketCheckIns() {
        return this.collectionSupermarketCheckIns;
    }

    getMensajes() {
        return this.collectionMensajes;
    }

    getUsers() {
        return this.collectionUsers;
    }

    getSections() {
        return this.collectionSections;
    }

    setResponse(id, status, nombre, fotoUsuario) {
        //en doc va el ID de la solicitud que respondo
        if (status == 0) {
            return this.collectionMensajes.doc(id).update({ status: 1, atiende: nombre, foto: fotoUsuario });
        }
        else if (status == 1) {
            return this.collectionMensajes.doc(id).update({ status: 2 });
        }
        else {
            return this.collectionMensajes.doc(id).update({ status: -1 });
        }
    }

    getSectionSearch() {
        return this.collectionSectionSearch;
    }

    getProducts() {
        return this.collectionProductos;
    }

    addSupermarket(name, phone, lat, lon, onAddedCallback) {
        this.collectionSuperMercados.add({
            name: name,
            phone: phone,
            location: new firebase.firestore.GeoPoint(lat, lon),
            shouldUseDirection: false
        }).then(function (docRef) {
            onAddedCallback(docRef);
        }).catch(function (error) {
            console.error("error agregando", error);
        })
    }

    addProduct(name, dPrice, iso, descripcion, description, imagenUrl, idSupermercado) {
        this.price = dPrice;
        this.collectionProductos.add({
            name: name,
            price: {
                iso: iso,
                units: this.price
            },
            descripcion: descripcion,
            description: description,
            imageUrl: imagenUrl,
            supermarkets: [
                this.collectionSuperMercados.doc(idSupermercado)
            ]
        }).then(function (docRef) {
            console.log("Se pudo escribir");
        }).catch(function (error) {
            console.error("error agregando", error);
        });
    }

    getSupermarkets() {
        return this.collectionSuperMercados;
    }

    getSupermarketSections(supermarketId) {
        return this.collectionSections.where("supermarket", "==", this.collectionSuperMercados.doc(supermarketId));
    }

    getSupermarketEdges(supermarketId) {
        return this.collectionEdges.where("supermarket", "==", this.collectionSuperMercados.doc(supermarketId));
    }

    getSupermarketProducts(supermarketId) {
        return this.collectionProductos.where("supermarkets", "array-contains", this.collectionSuperMercados.doc(supermarketId));
    }

    addSections(supermarketId, newSection, onAddedCallback) {
        this.collectionSections.add({
            name: newSection.name,
            imageUrl: newSection.imageUrl,
            beaconUuid: newSection.beaconUuid,
            supermarket: this.collectionSuperMercados.doc(supermarketId)
        }).then(function (docRef) {
            onAddedCallback(docRef);
        }).catch(function (error) {
            console.error("error agregando", error);
        });
    }

    addEdge(supermarketId, newEdge, onAddedCallback) {
        this.collectionEdges.add({
            bearing: newEdge.bearing,
            direction: newEdge.direction,
            distance: newEdge.distance,
            from: newEdge.from,
            to: newEdge.to,
            unit: "steps",
            supermarket: this.collectionSuperMercados.doc(supermarketId)
        }).then(function (docRef) {
            onAddedCallback(docRef);
        }).catch(function (error) {
            console.error("error agregando", error);
        });
    }

    deleteSection(sectionId, onAddedCallback) {
        this.collectionSections.doc(sectionId).delete()
            .then(function () {
                onAddedCallback();
            }).catch(function (error) {
                console.error("error agregando", error);
            });
    }

    deleteEdge(edgeId, onAddedCallback) {
        this.collectionEdges.doc(edgeId).delete()
            .then(function () {
                onAddedCallback();
            }).catch(function (error) {
                console.error("error agregando", error);
            });
    }

    deleteProduct(productId, onAddedCallback) {
        this.collectionProductos.doc(productId).delete()
            .then(function () {
                onAddedCallback();
            }).catch(function (error) {
                console.error("error agregando", error);
            });
    }

    deleteSupermarket(supermarketId, onAddedCallback) {
        this.collectionSuperMercados.doc(supermarketId).delete()
            .then(function () {
                onAddedCallback();
            }).catch(function (error) {
                console.error("error agregando", error);
            });
    }
}
