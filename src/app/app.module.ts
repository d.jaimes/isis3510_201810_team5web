import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { StorageServiceModule } from 'angular-webstorage-service';

import { AppComponent } from './app.component';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireModule } from '@angular/fire';
import { environment } from '../environments/environment';
import { FireStoreService } from './firestore.service';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { HomePageComponent } from './componentes/home-page/home-page.component';
import { NavbarComponent } from './componentes/navbar/navbar.component';
import { RegisterPageComponent } from './componentes/register-page/register-page.component';
import { LoginPageComponent } from './componentes/login-page/login-page.component';
import { PrivadoPageComponent } from './componentes/privado-page/privado-page.component';
import { NotFoundPageComponent } from './componentes/not-found-page/not-found-page.component';
import { AssistanceComponent } from './componentes/assistance/assistance.component';
import { AppRoutingModule } from './/app-routing.module';
import { EstadisticasComponent } from './componentes/estadisticas/estadisticas.component';
import { MapeoComponent } from './componentes/mapeo/mapeo.component';
import {SupermarketDetailComponent} from './componentes/supermarket-detail/supermarket-detail.component';
import{AuthService} from './servicios/auth.service';
import{ FormsModule } from '@angular/forms';
import { UsuarioComponent } from './componentes/usuario/usuario.component';
//import {AngularFireModule} from 'angularfire2';
import{AngularFireDatabase} from 'angularfire2/database';
import { GoogleChartsModule } from 'angular-google-charts';


@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    NavbarComponent,
    RegisterPageComponent,
    LoginPageComponent,
    PrivadoPageComponent,
    NotFoundPageComponent,
    AssistanceComponent,
    EstadisticasComponent,
    MapeoComponent,
    UsuarioComponent,
    SupermarketDetailComponent
  ],
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    AngularFireAuthModule,
    AppRoutingModule,
    FormsModule,
    StorageServiceModule,
    GoogleChartsModule.forRoot()
  ],
  providers: [
      AngularFirestore,
      FireStoreService,
      AuthService,
      AngularFireDatabase
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
