import { Injectable } from '@angular/core';
import {AngularFireAuth} from 'angularfire2/auth'
import* as fibase from 'firebase/app'
import { auth } from 'firebase/app';
//import {map, catchError} from "rxjs/operators";
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Observable, of } from 'rxjs';
import { switchMap} from 'rxjs/operators';
import { Router } from '@angular/router';
import { map} from 'rxjs/operators';

interface User
 {
  uid: string;
  email: string;
  photoURL?: string;
  displayName?: string;
  telefono?: string;
  
}

@Injectable({ providedIn: 'root' })
export class AuthService
 {
  user: Observable<User>;
  
  constructor(public afAuth: AngularFireAuth,   private afs: AngularFirestore,
    private router: Router  ) 
    {
  /// Get auth data, then get firestore user document || null
  this.user = this.afAuth.authState.pipe(
    switchMap(user => {
      if (user) {
        return this.afs.doc<User>(`users/${user.uid}`).valueChanges()
      } else {
        return of(null)
      }
    })
  )
}
googleLogin() {
  return this.afAuth.auth.signInWithPopup( new fibase.auth.GoogleAuthProvider).then((credential) => {
    this.updateUserData(credential.user)
  })
}

  registerUser(email: string, password: string)
{
  return new Promise((resolve, reject)=>{
    this.afAuth.auth.createUserWithEmailAndPassword(email,password)
    .then(userData=>resolve(userData),
  err => reject(err));
  });

}
  
logIn(email: string, password: string)
{
  return new Promise((resolve, reject)=>{
    this.afAuth.auth.signInWithEmailAndPassword(email,password)
    .then(userData=>resolve(userData),
  err => reject(err));
  });

}

// TODO: solucionar mapa 

getAuth()
{
 return this.afAuth.authState.pipe(map(auth=>auth));
}
private updateUserData(user) {
  // Sets user data to firestore on login

  const userRef: AngularFirestoreDocument<any> = this.afs.doc(`users/${user.uid}`);

  const data: User = {
    uid: user.uid,
    email: user.email,
    displayName: user.displayName,
    photoURL: user.photoURL,
    telefono: user.phoneNumber
  }

  return userRef.set(data, { merge: true })

}

  logout()
  {
    return this.afAuth.auth.signOut();
  }

}
