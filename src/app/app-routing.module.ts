import { NgModule } from '@angular/core';
import{Routes, RouterModule} from '@angular/router';
import { CommonModule } from '@angular/common';
import {HomePageComponent} from './componentes/home-page/home-page.component';
import {LoginPageComponent} from './componentes/login-page/login-page.component';
import {AssistanceComponent} from './componentes/assistance/assistance.component';
import {NotFoundPageComponent} from './componentes/not-found-page/not-found-page.component';
import {PrivadoPageComponent} from './componentes/privado-page/privado-page.component';
import {RegisterPageComponent} from './componentes/register-page/register-page.component';
import {EstadisticasComponent} from './componentes/estadisticas/estadisticas.component';
import {MapeoComponent} from './componentes/mapeo/mapeo.component';
import {SupermarketDetailComponent} from './componentes/supermarket-detail/supermarket-detail.component';

const routes : Routes=[
        {path:'',component: HomePageComponent},
        {path:'login', component: LoginPageComponent},
        {path:'register', component: RegisterPageComponent},
        {path:'private', component: PrivadoPageComponent},
        {path:'assistance', component: AssistanceComponent},
        {path:'estadistica', component: EstadisticasComponent},
        {path:'mapeo', component: MapeoComponent},
        {path:'supermaket_detail', component: SupermarketDetailComponent},
        {path:'**', component: NotFoundPageComponent}
];
@NgModule({
  imports : [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  declarations: []
})
export class AppRoutingModule { }
